using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Balancika.Dal;

namespace Balancika.Bll.Base
{
	public class TempLedgerReportTableBase
	{
		protected static Balancika.Dal.TempLedgerReportTableDal dal = new Balancika.Dal.TempLedgerReportTableDal();

		public System.Int32 VoucherTempId		{ get ; set; }

		public System.String VoucherTrasactionDate		{ get ; set; }

		public System.String MasterAccount		{ get ; set; }

		public System.Int32 MasterAccountCOAid		{ get ; set; }

		public System.String ContraAccount		{ get ; set; }

		public System.Int32 ContraAccountId		{ get ; set; }

		public System.Decimal OpeningBalance		{ get ; set; }

		public System.Decimal Debit		{ get ; set; }

		public System.Decimal Credit		{ get ; set; }

		public System.Decimal ClosingBalance		{ get ; set; }

		public System.String VoucherType		{ get ; set; }

		public System.String TransactionCode		{ get ; set; }


		public  Int32 InsertTempLedgerReportTable()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherTempId", VoucherTempId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@VoucherTrasactionDate", VoucherTrasactionDate);
			lstItems.Add("@MasterAccount", MasterAccount);
			lstItems.Add("@MasterAccountCOAid", MasterAccountCOAid.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@ContraAccount", ContraAccount);
			lstItems.Add("@ContraAccountId", ContraAccountId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@OpeningBalance", OpeningBalance.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@Debit", Debit.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@Credit", Credit.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@ClosingBalance", ClosingBalance.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@VoucherType", VoucherType);
			lstItems.Add("@TransactionCode", TransactionCode);

			return dal.InsertTempLedgerReportTable(lstItems);
		}

		public  Int32 UpdateTempLedgerReportTable()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherTempId", VoucherTempId.ToString());
			lstItems.Add("@VoucherTrasactionDate", VoucherTrasactionDate);
			lstItems.Add("@MasterAccount", MasterAccount);
			lstItems.Add("@MasterAccountCOAid", MasterAccountCOAid.ToString());
			lstItems.Add("@ContraAccount", ContraAccount);
			lstItems.Add("@ContraAccountId", ContraAccountId.ToString());
			lstItems.Add("@OpeningBalance", OpeningBalance.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@Debit", Debit.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@Credit", Credit.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@ClosingBalance", ClosingBalance.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@VoucherType", VoucherType);
			lstItems.Add("@TransactionCode", TransactionCode);

			return dal.UpdateTempLedgerReportTable(lstItems);
		}

		public  Int32 DeleteTempLedgerReportTableByVoucherTempId(Int32 VoucherTempId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherTempId", VoucherTempId);

			return dal.DeleteTempLedgerReportTableByVoucherTempId(lstItems);
		}

		public List<TempLedgerReportTable> GetAllTempLedgerReportTable()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllTempLedgerReportTable(lstItems);
			List<TempLedgerReportTable> TempLedgerReportTableList = new List<TempLedgerReportTable>();
			foreach (DataRow dr in dt.Rows)
			{
				TempLedgerReportTableList.Add(GetObject(dr));
			}
			return TempLedgerReportTableList;
		}

		public TempLedgerReportTable  GetTempLedgerReportTableByVoucherTempId(Int32 _VoucherTempId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@VoucherTempId", _VoucherTempId);

			DataTable dt = dal.GetTempLedgerReportTableByVoucherTempId(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  TempLedgerReportTable GetObject(DataRow dr)
		{

			TempLedgerReportTable objTempLedgerReportTable = new TempLedgerReportTable();
			objTempLedgerReportTable.VoucherTempId = (dr["VoucherTempId"] == DBNull.Value) ? 0 : (Int32)dr["VoucherTempId"];
			objTempLedgerReportTable.VoucherTrasactionDate = (dr["VoucherTrasactionDate"] == DBNull.Value) ? "" : (String)dr["VoucherTrasactionDate"].ToString();
			objTempLedgerReportTable.MasterAccount = (dr["MasterAccount"] == DBNull.Value) ? "" : (String)dr["MasterAccount"];
			objTempLedgerReportTable.MasterAccountCOAid = (dr["MasterAccountCOAid"] == DBNull.Value) ? 0 : (Int32)dr["MasterAccountCOAid"];
			objTempLedgerReportTable.ContraAccount = (dr["ContraAccount"] == DBNull.Value) ? "" : (String)dr["ContraAccount"];
			objTempLedgerReportTable.ContraAccountId = (dr["ContraAccountId"] == DBNull.Value) ? 0 : (Int32)dr["ContraAccountId"];
			objTempLedgerReportTable.OpeningBalance = (dr["OpeningBalance"] == DBNull.Value) ? 0 : (Decimal)dr["OpeningBalance"];
			objTempLedgerReportTable.Debit = (dr["Debit"] == DBNull.Value) ? 0 : (Decimal)dr["Debit"];
			objTempLedgerReportTable.Credit = (dr["Credit"] == DBNull.Value) ? 0 : (Decimal)dr["Credit"];
			objTempLedgerReportTable.ClosingBalance = (dr["ClosingBalance"] == DBNull.Value) ? 0 : (Decimal)dr["ClosingBalance"];
			objTempLedgerReportTable.VoucherType = (dr["VoucherType"] == DBNull.Value) ? "" : (String)dr["VoucherType"];
			objTempLedgerReportTable.TransactionCode = (dr["TransactionCode"] == DBNull.Value) ? "" : (String)dr["TransactionCode"];

			return objTempLedgerReportTable;
		}
	}
}
