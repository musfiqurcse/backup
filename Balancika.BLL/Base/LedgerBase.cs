using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Balancika.Dal;

namespace Balancika.Bll.Base
{
	public class LedgerBase
	{
		protected static Balancika.Dal.LedgerDal dal = new Balancika.Dal.LedgerDal();

		public System.Int32 LedgerId		{ get ; set; }

		public System.String TransactionNo		{ get ; set; }

		public System.DateTime TransactionDate		{ get ; set; }

		public System.String VoucherType		{ get ; set; }

		public System.Int64 VoucherId		{ get ; set; }

		public System.String AccountNo		{ get ; set; }

		public System.String ContraAccountNo		{ get ; set; }

		public System.Decimal Debit		{ get ; set; }

		public System.Decimal Credit		{ get ; set; }

		public System.String Description		{ get ; set; }

		public System.Int32 UserId		{ get ; set; }

		public System.DateTime LastUpdateTime		{ get ; set; }

		public System.Int64 CompanyId		{ get ; set; }


		public  Int32 InsertLedger()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@LedgerId", LedgerId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@TransactionNo", TransactionNo);
			lstItems.Add("@TransactionDate", TransactionDate.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@VoucherType", VoucherType);
			lstItems.Add("@VoucherId", VoucherId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@AccountNo", AccountNo);
			lstItems.Add("@ContraAccountNo", ContraAccountNo);
			lstItems.Add("@Debit", Debit.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@Credit", Credit.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@Description", Description);
			lstItems.Add("@UserId", UserId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@LastUpdateTime", LastUpdateTime.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@CompanyId", CompanyId.ToString(CultureInfo.InvariantCulture));

			return dal.InsertLedger(lstItems);
		}

		public  Int32 UpdateLedger()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@LedgerId", LedgerId.ToString());
			lstItems.Add("@TransactionNo", TransactionNo);
			lstItems.Add("@TransactionDate", TransactionDate.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@VoucherType", VoucherType);
			lstItems.Add("@VoucherId", VoucherId.ToString());
			lstItems.Add("@AccountNo", AccountNo);
			lstItems.Add("@ContraAccountNo", ContraAccountNo);
			lstItems.Add("@Debit", Debit.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@Credit", Credit.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@Description", Description);
			lstItems.Add("@UserId", UserId.ToString());
			lstItems.Add("@LastUpdateTime", LastUpdateTime.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@CompanyId", CompanyId.ToString());

			return dal.UpdateLedger(lstItems);
		}

		public  Int32 DeleteLedgerByLedgerId(Int32 LedgerId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@LedgerId", LedgerId);

			return dal.DeleteLedgerByLedgerId(lstItems);
		}

		public List<Ledger> GetAllLedger(int CompanyId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@CompanyId", CompanyId);
			DataTable dt = dal.GetAllLedger(lstItems);
			List<Ledger> LedgerList = new List<Ledger>();
			foreach (DataRow dr in dt.Rows)
			{
				LedgerList.Add(GetObject(dr));
			}
			return LedgerList;
		}

		public Ledger GetLedgerByLedgerId(int _LedgerId,int _CompanyId)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@LedgerId", _LedgerId);
			lstItems.Add("@CompanyId", _CompanyId);

			DataTable dt = dal.GetLedgerByLedgerId(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  Ledger GetObject(DataRow dr)
		{

			Ledger objLedger = new Ledger();
			objLedger.LedgerId = (dr["LedgerId"] == DBNull.Value) ? 0 : (Int32)dr["LedgerId"];
			objLedger.TransactionNo = (dr["TransactionNo"] == DBNull.Value) ? "" : (String)dr["TransactionNo"];
			objLedger.TransactionDate = (dr["TransactionDate"] == DBNull.Value) ? DateTime.MinValue : (DateTime)dr["TransactionDate"];
			objLedger.VoucherType = (dr["VoucherType"] == DBNull.Value) ? "" : (String)dr["VoucherType"];
			objLedger.VoucherId = (dr["VoucherId"] == DBNull.Value) ? 0 : (Int64)dr["VoucherId"];
			objLedger.AccountNo = (dr["AccountNo"] == DBNull.Value) ? "" : (String)dr["AccountNo"];
			objLedger.ContraAccountNo = (dr["ContraAccountNo"] == DBNull.Value) ? "" : (String)dr["ContraAccountNo"];
			objLedger.Debit = (dr["Debit"] == DBNull.Value) ? 0 : (Decimal)dr["Debit"];
			objLedger.Credit = (dr["Credit"] == DBNull.Value) ? 0 : (Decimal)dr["Credit"];
			objLedger.Description = (dr["Description"] == DBNull.Value) ? "" : (String)dr["Description"];
			objLedger.UserId = (dr["UserId"] == DBNull.Value) ? 0 : (Int32)dr["UserId"];
			objLedger.LastUpdateTime = (dr["LastUpdateTime"] == DBNull.Value) ? DateTime.MinValue : (DateTime)dr["LastUpdateTime"];
			objLedger.CompanyId = (dr["CompanyId"] == DBNull.Value) ? 0 : (Int64)dr["CompanyId"];

			return objLedger;
		}
	}
}
