using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Balancika.Dal;

namespace Balancika.Bll.Base
{
	public class VoucherDetailTempBase
	{
		protected static Balancika.Dal.VoucherDetailTempDal dal = new Balancika.Dal.VoucherDetailTempDal();

		public System.Int32 Id		{ get ; set; }

		public System.Int64 userId		{ get ; set; }

		public System.Int32 COAId		{ get ; set; }

		public System.String LineDesciption		{ get ; set; }

		public System.Decimal Amount		{ get ; set; }


		public  Int32 InsertVoucherDetailTemp()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@userId", userId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@COAId", COAId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@LineDesciption", LineDesciption);
			lstItems.Add("@Amount", Amount.ToString(CultureInfo.InvariantCulture));

			return dal.InsertVoucherDetailTemp(lstItems);
		}

		public  Int32 UpdateVoucherDetailTemp()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString());
			lstItems.Add("@userId", userId.ToString());
			lstItems.Add("@COAId", COAId.ToString());
			lstItems.Add("@LineDesciption", LineDesciption);
			lstItems.Add("@Amount", Amount.ToString(CultureInfo.InvariantCulture));

			return dal.UpdateVoucherDetailTemp(lstItems);
		}

		public  Int32 DeleteVoucherDetailTempById(Int32 Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id);

			return dal.DeleteVoucherDetailTempById(lstItems);
		}
        public Int32 DeleteAllVoucherDetailsTempbyUserId(Int64 userId)
        {
            Hashtable lstItems = new Hashtable();
            lstItems.Add("@userId", userId);

            return dal.DeleteAllVoucherDetailsTempByUserId(lstItems);
        }

		public List<VoucherDetailTemp> GetAllVoucherDetailTemp()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllVoucherDetailTemp(lstItems);
			List<VoucherDetailTemp> VoucherDetailTempList = new List<VoucherDetailTemp>();
			foreach (DataRow dr in dt.Rows)
			{
				VoucherDetailTempList.Add(GetObject(dr));
			}
			return VoucherDetailTempList;
		}
        public List<VoucherDetailTemp> GetAllVoucherDetailTempByUserId(Int32 Id)
        {
            Hashtable lstItems = new Hashtable();
            lstItems.Add("@userId", Id);
            DataTable dt = dal.GetAllVoucherDetailTempByUserId(lstItems);
            
            List<VoucherDetailTemp> VoucherDetailTempList = new List<VoucherDetailTemp>();
            foreach (DataRow dr in dt.Rows)
            {
                VoucherDetailTempList.Add(GetObject(dr));
            }
            return VoucherDetailTempList;
        }

		public VoucherDetailTemp  GetVoucherDetailTempById(Int32 _Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", _Id);

			DataTable dt = dal.GetVoucherDetailTempById(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  VoucherDetailTemp GetObject(DataRow dr)
		{

			VoucherDetailTemp objVoucherDetailTemp = new VoucherDetailTemp();
			objVoucherDetailTemp.Id = (dr["Id"] == DBNull.Value) ? 0 : (Int32)dr["Id"];
			objVoucherDetailTemp.userId = (dr["userId"] == DBNull.Value) ? 0 : (Int64)dr["userId"];
			objVoucherDetailTemp.COAId = (dr["COAId"] == DBNull.Value) ? 0 : (Int32)dr["COAId"];
			objVoucherDetailTemp.LineDesciption = (dr["LineDesciption"] == DBNull.Value) ? "" : (String)dr["LineDesciption"];
			objVoucherDetailTemp.Amount = (dr["Amount"] == DBNull.Value) ? 0 : (Decimal)dr["Amount"];

			return objVoucherDetailTemp;
		}
	}
}
