using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Bll.Base;

namespace Balancika.Bll
{
	public class TempLedgerReportTable : Balancika.Bll.Base.TempLedgerReportTableBase
	{
		private static Balancika.Dal.TempLedgerReportTableDal Dal = new Balancika.Dal.TempLedgerReportTableDal();
		public TempLedgerReportTable() : base()
		{
		}

	    public int GetMaxTempLedgerIdByMasterAccountId(int masterId)
	    {
	        return Dal.GetMaxTempLedgerIdByMasterAccountId(masterId);
	    }

	    public int GetMaximumTempLedgerReportTableId()
	    {
	        return Dal.GetMaximumTempLedgerReportTableID();
	    }
	}
}
