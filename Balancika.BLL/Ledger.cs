using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Bll.Base;

namespace Balancika.Bll
{
	public class Ledger : Balancika.Bll.Base.LedgerBase
	{
		private static Balancika.Dal.LedgerDal Dal = new Balancika.Dal.LedgerDal();
		public Ledger() : base()
		{
		}
        public int GetMaxLedgerId()
        {
            return dal.GetMaximumLedgerID();
        }
        public DataSet GetLedgerDatasetByCompanyId(Int32 CompanyId)
        {
            return dal.GetDataSetByCompanyId(CompanyId);
        }
	}
}
