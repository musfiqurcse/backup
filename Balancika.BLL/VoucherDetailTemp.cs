using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Bll.Base;

namespace Balancika.Bll
{
	public class VoucherDetailTemp : Balancika.Bll.Base.VoucherDetailTempBase
	{
		private static Balancika.Dal.VoucherDetailTempDal Dal = new Balancika.Dal.VoucherDetailTempDal();
		public VoucherDetailTemp() : base()
		{
		}
        public int GetMaxVoucherDetailTempId()
        {
            return dal.GetMaximumVoucherDetailTempId();
        }

	    
	}
}
