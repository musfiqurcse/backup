﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Balancika.Bll;
using Balancika.BLL;
using BALANCIKA.BLL;
using Telerik.Web.UI;

namespace Balancika.WebApplication
{
    public partial class VoucherLists : System.Web.UI.Page
    {
        private bool isNewEntry;
        private Users user;
        private Company _company = new Company();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["customVoucherMessage"] != null)
            {
                string msg = (string) Session["customVoucherMessage"];
                Alert.Show(msg);
                Session["customVoucherMessage"] = null;
            }
            _company = (Company) Session["Company"];
            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx");
                }
                else
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx?" + str);
                }

            }
            if (!IsPostBack)
            {
                this.LoadFirstTime();
            }
        }

        private void LoadFirstTime()
        {
            RadGrid1.DataSource = new VoucherMaster().GetAllVoucherMaster(_company.CompanyId);
            RadGrid1.DataBind();
        }

        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;

            }
            user = (Users) Session["user"];
            return user.UserId != 0;

        }

        protected void btnLEdgerPosting_Click(object sender, EventArgs e)
        {
            try
            {
                VoucherMaster objVoucherMaster = new VoucherMaster();

                var receiptDataList =
                    objVoucherMaster.GetAllVoucherMaster(_company.CompanyId)
                        .Where(d => d.VoucherType == "Receipt" && d.IsPosted == false && d.SourceOfAccount == "Voucher")
                        .ToList();
                var expenseDataList =
                    objVoucherMaster.GetAllVoucherMaster(_company.CompanyId)
                        .Where(d => d.VoucherType == "Expense" && d.IsPosted == false && d.SourceOfAccount == "Voucher")
                        .ToList();
                var receiptChequeList =
                    objVoucherMaster.GetAllVoucherMaster(_company.CompanyId)
                        .Where(d => d.VoucherType == "Receipt" && d.IsPosted == false && d.SourceOfAccount == "Cheque")
                        .ToList();
                var expenseChequeList =
                    objVoucherMaster.GetAllVoucherMaster(_company.CompanyId)
                        .Where(d => d.VoucherType == "Expense" && d.IsPosted == false && d.SourceOfAccount == "Cheque")
                        .ToList();
                List<ChartOfAccount> coaList = new ChartOfAccount().GetAllChartOfAccount(_company.CompanyId);

                ChartOfAccount temp11 = new ChartOfAccount();

                if (receiptDataList.Count != 0)
                {
                    foreach (var item in receiptDataList)
                    {
                        var detailsItem = new VoucherDetail().GetAllVoucherDetailByVoucherMasterId(item.VoucherId);
                        TempLedgerReportTable newTempLedger = new TempLedgerReportTable();
                        newTempLedger.VoucherTrasactionDate = item.VoucherDate;
                        newTempLedger.MasterAccountCOAid = item.SourceCOAId;
                        var temp1 = temp11.GetChartOfAccountByCoaId(item.SourceCOAId, _company.CompanyId);
                        newTempLedger.MasterAccount = temp1.CoaTitle;
                        newTempLedger.VoucherType = "Receipt";
                        newTempLedger.TransactionCode = item.VoucherNo;
                        foreach (var singleItem in detailsItem)
                        {
                            decimal d = 0;
                            Ledger objLedger = new Ledger();
                            objLedger.VoucherId = item.VoucherId;
                            objLedger.VoucherType = item.VoucherType;
                            objLedger.AccountNo = singleItem.COAId.ToString();
                            objLedger.LedgerId = objLedger.GetMaxLedgerId() + 1;
                            objLedger.TransactionNo = item.VoucherNo;
                            objLedger.TransactionDate = Convert.ToDateTime(ledgerPostingDatePicker.SelectedDate);
                            //  objLedger.TransactionDate = item.VoucherDate;
                            objLedger.LastUpdateTime = DateTime.Now;
                            objLedger.UserId = user.UserId;
                            objLedger.Description = item.VoucherDescription;
                            objLedger.Debit = d;
                            objLedger.Credit = singleItem.Amount;
                            objLedger.ContraAccountNo = item.SourceCOAId.ToString();
                            objLedger.CompanyId = _company.CompanyId;
                            objLedger.InsertLedger();
                            objLedger = new Ledger();
                            objLedger.VoucherId = item.VoucherId;
                            objLedger.VoucherType = item.VoucherType;
                            objLedger.AccountNo = item.SourceCOAId.ToString();
                            objLedger.LedgerId = objLedger.GetMaxLedgerId() + 1;
                            objLedger.TransactionNo = item.VoucherNo;
                            objLedger.TransactionDate = Convert.ToDateTime(ledgerPostingDatePicker.SelectedDate);
                            objLedger.LastUpdateTime = DateTime.Now;
                            objLedger.UserId = user.UserId;
                            objLedger.Description = item.VoucherDescription;
                            objLedger.Debit = Convert.ToDecimal(singleItem.Amount);
                            objLedger.Credit = 0;
                            objLedger.ContraAccountNo = singleItem.COAId.ToString();
                            objLedger.CompanyId = _company.CompanyId;
                            objLedger.InsertLedger();
                            newTempLedger.Debit = singleItem.Amount;
                            newTempLedger.Credit = 0;


                            newTempLedger.ContraAccountId = singleItem.COAId;
                            var temp2 = temp11.GetChartOfAccountByCoaId(singleItem.COAId, _company.CompanyId);
                            newTempLedger.ContraAccount = temp2.CoaTitle;
                            if (newTempLedger.GetAllTempLedgerReportTable().Count > 0 && (
                                newTempLedger.GetAllTempLedgerReportTable()
                                    .Any(pp => pp.MasterAccountCOAid == item.SourceCOAId)))
                            {
                                int id =
                                    new TempLedgerReportTable().GetMaxTempLedgerIdByMasterAccountId(item.SourceCOAId);
                                var item22 = new TempLedgerReportTable().GetTempLedgerReportTableByVoucherTempId(id);
                                newTempLedger.OpeningBalance = item22.ClosingBalance;
                                newTempLedger.ClosingBalance = (newTempLedger.OpeningBalance + newTempLedger.Debit);
                                newTempLedger.VoucherTempId =
                                    new TempLedgerReportTable().GetMaximumTempLedgerReportTableId() + 1;
                                newTempLedger.InsertTempLedgerReportTable();

                            }
                            else
                            {

                                newTempLedger.ClosingBalance = newTempLedger.Debit;
                                newTempLedger.VoucherTempId =
                                    new TempLedgerReportTable().GetMaximumTempLedgerReportTableId() + 1;
                                newTempLedger.InsertTempLedgerReportTable();
                            }




                        }

                        //objVoucherMaster.VoucherId = item.VoucherId;
                        //objVoucherMaster.IsPosted = true;
                        //objVoucherMaster.InsertVoucherMaster();

                        item.IsPosted = true;
                        item.UpdateVoucherMaster();
                    }
                }
                else
                {
                    Alert.Show("No Data Found for Posting");
                }

                if (expenseDataList.Count != 0)
                {
                    foreach (var item in expenseDataList)
                    {
                        var detailsItem = new VoucherDetail().GetAllVoucherDetailByVoucherMasterId(item.VoucherId);
                        TempLedgerReportTable newTempLedger = new TempLedgerReportTable();
                        newTempLedger.VoucherTrasactionDate = item.VoucherDate;
                        newTempLedger.MasterAccountCOAid = item.SourceCOAId;
                        var temp1 = temp11.GetChartOfAccountByCoaId(item.SourceCOAId, _company.CompanyId);
                        newTempLedger.MasterAccount = temp1.CoaTitle;
                        newTempLedger.VoucherType = "Receipt";
                        newTempLedger.TransactionCode = item.VoucherNo;
                        foreach (var singleItem in detailsItem)
                        {
                            decimal d = 0;
                            Ledger objLedger = new Ledger();
                            objLedger.VoucherId = item.VoucherId;
                            objLedger.VoucherType = item.VoucherType;
                            objLedger.AccountNo = singleItem.COAId.ToString();
                            objLedger.LedgerId = objLedger.GetMaxLedgerId() + 1;
                            objLedger.TransactionNo = item.VoucherNo;
                            objLedger.TransactionDate = Convert.ToDateTime(ledgerPostingDatePicker.SelectedDate);
                            objLedger.LastUpdateTime = DateTime.Now;
                            objLedger.UserId = user.UserId;
                            objLedger.Description = item.VoucherDescription;
                            objLedger.Debit = singleItem.Amount;
                            objLedger.Credit = d;
                            objLedger.ContraAccountNo = item.SourceCOAId.ToString();
                            objLedger.CompanyId = _company.CompanyId;
                            objLedger.InsertLedger();
                            objLedger = new Ledger();
                            objLedger.VoucherId = item.VoucherId;
                            objLedger.VoucherType = item.VoucherType;
                            objLedger.AccountNo = item.SourceCOAId.ToString();
                            objLedger.LedgerId = objLedger.GetMaxLedgerId() + 1;
                            objLedger.TransactionNo = item.VoucherNo;
                            objLedger.TransactionDate = Convert.ToDateTime(ledgerPostingDatePicker.SelectedDate);
                            objLedger.LastUpdateTime = DateTime.Now;
                            objLedger.UserId = user.UserId;
                            objLedger.Description = item.VoucherDescription;
                            objLedger.Debit = d;
                            objLedger.Credit = singleItem.Amount;
                            objLedger.ContraAccountNo = singleItem.COAId.ToString();
                            objLedger.CompanyId = _company.CompanyId;
                            objLedger.InsertLedger();
                            newTempLedger.Debit = 0;
                            newTempLedger.Credit = singleItem.Amount;


                            newTempLedger.ContraAccountId = singleItem.COAId;
                            var temp2 = temp11.GetChartOfAccountByCoaId(singleItem.COAId, _company.CompanyId);
                            newTempLedger.ContraAccount = temp2.CoaTitle;
                            if (newTempLedger.GetAllTempLedgerReportTable().Count > 0 && (
                                newTempLedger.GetAllTempLedgerReportTable()
                                    .Any(pp => pp.MasterAccountCOAid == item.SourceCOAId)))
                            {
                                int id =
                                    new TempLedgerReportTable().GetMaxTempLedgerIdByMasterAccountId(item.SourceCOAId);
                                var item22 = new TempLedgerReportTable().GetTempLedgerReportTableByVoucherTempId(id);
                                newTempLedger.OpeningBalance = item22.ClosingBalance;
                                newTempLedger.ClosingBalance = (newTempLedger.OpeningBalance - newTempLedger.Credit);
                                newTempLedger.VoucherTempId =
                                    new TempLedgerReportTable().GetMaximumTempLedgerReportTableId() + 1;
                                newTempLedger.InsertTempLedgerReportTable();

                            }
                            else
                            {

                                newTempLedger.ClosingBalance = -newTempLedger.Credit;
                                newTempLedger.VoucherTempId =
                                    new TempLedgerReportTable().GetMaximumTempLedgerReportTableId() + 1;
                                newTempLedger.InsertTempLedgerReportTable();
                            }



                        }

                        //objVoucherMaster.VoucherId = item.VoucherId;
                        //objVoucherMaster.IsPosted = true;
                        //objVoucherMaster.InsertVoucherMaster();

                        item.IsPosted = true;
                        item.UpdateVoucherMaster();
                    }
                }
                if (receiptChequeList.Count != 0)
                {
                    foreach (var item in receiptChequeList)
                    {
                        // var detailsItem = new VoucherDetail().GetAllVoucherDetailByVoucherMasterId(item.VoucherId);
                        var chequeItem = new VoucherChequeInfo().GetVoucherChequeInfoByVoucherId(item.VoucherId);


                        decimal d = 0;
                        Ledger objLedger = new Ledger();
                        objLedger.VoucherId = item.VoucherId;
                        objLedger.VoucherType = item.VoucherType;
                        objLedger.AccountNo = item.SourceCOAId.ToString();
                        objLedger.LedgerId = objLedger.GetMaxLedgerId() + 1;
                        objLedger.TransactionNo = item.VoucherNo;
                        objLedger.TransactionDate = Convert.ToDateTime(ledgerPostingDatePicker.SelectedDate);
                        //  objLedger.TransactionDate = item.VoucherDate;
                        objLedger.LastUpdateTime = DateTime.Now;
                        objLedger.UserId = item.UpdateBy;
                        objLedger.Description = item.VoucherDescription;
                        objLedger.Debit = d;
                        objLedger.Credit = Convert.ToDecimal(item.VoucherTotal);
                        objLedger.ContraAccountNo = Convert.ToString(2);
                        objLedger.CompanyId = _company.CompanyId;
                        objLedger.InsertLedger();
                        objLedger = new Ledger();
                        objLedger.VoucherId = item.VoucherId;
                        objLedger.VoucherType = item.VoucherType;
                        objLedger.AccountNo = Convert.ToString(2);
                        objLedger.LedgerId = objLedger.GetMaxLedgerId() + 1;
                        objLedger.TransactionNo = item.VoucherNo;
                        objLedger.TransactionDate = Convert.ToDateTime(ledgerPostingDatePicker.SelectedDate);
                        objLedger.LastUpdateTime = DateTime.Now;
                        objLedger.UserId = item.UpdateBy;
                        objLedger.Description = item.VoucherDescription;
                        objLedger.Debit = Convert.ToDecimal(item.VoucherTotal);
                        objLedger.Credit = 0;
                        objLedger.ContraAccountNo = item.SourceCOAId.ToString();
                        objLedger.CompanyId = _company.CompanyId;
                        objLedger.InsertLedger();
                        TempLedgerReportTable newTempLedger = new TempLedgerReportTable();
                        newTempLedger.VoucherTrasactionDate = item.VoucherDate;
                        newTempLedger.MasterAccountCOAid = 2;
                        var temp1 = temp11.GetChartOfAccountByCoaId(newTempLedger.MasterAccountCOAid, _company.CompanyId);
                        newTempLedger.MasterAccount = temp1.CoaTitle;
                        newTempLedger.VoucherType = "Receipt";
                        newTempLedger.TransactionCode = item.VoucherNo;
                        newTempLedger.Debit = Convert.ToDecimal(item.VoucherTotal);
                        newTempLedger.Credit = 0;


                        newTempLedger.ContraAccountId = item.SourceCOAId;
                        var temp2 = temp11.GetChartOfAccountByCoaId(item.SourceCOAId, _company.CompanyId);
                        newTempLedger.ContraAccount = temp2.CoaTitle;
                        if (newTempLedger.GetAllTempLedgerReportTable().Count > 0 && (
                            newTempLedger.GetAllTempLedgerReportTable()
                                .Any(pp => pp.MasterAccountCOAid == item.SourceCOAId)))
                        {
                            int id = new TempLedgerReportTable().GetMaxTempLedgerIdByMasterAccountId(2);
                            var item22 = new TempLedgerReportTable().GetTempLedgerReportTableByVoucherTempId(id);
                            newTempLedger.OpeningBalance = item22.ClosingBalance;
                            newTempLedger.ClosingBalance = (newTempLedger.OpeningBalance + newTempLedger.Debit);
                            newTempLedger.VoucherTempId =
                                new TempLedgerReportTable().GetMaximumTempLedgerReportTableId() + 1;
                            newTempLedger.InsertTempLedgerReportTable();

                        }
                        else
                        {

                            newTempLedger.ClosingBalance = newTempLedger.Debit;
                            newTempLedger.VoucherTempId =
                                new TempLedgerReportTable().GetMaximumTempLedgerReportTableId() + 1;
                            newTempLedger.InsertTempLedgerReportTable();
                        }






                        //objVoucherMaster.VoucherId = item.VoucherId;
                        //objVoucherMaster.IsPosted = true;
                        //objVoucherMaster.InsertVoucherMaster();

                        item.IsPosted = true;
                        item.UpdateVoucherMaster();
                    }
                    if (expenseChequeList.Count != 0)
                    {
                        foreach (var item in expenseChequeList)
                        {
                            // var detailsItem = new VoucherDetail().GetAllVoucherDetailByVoucherMasterId(item.VoucherId);
                            var chequeItem = new VoucherChequeInfo().GetVoucherChequeInfoByVoucherId(item.VoucherId);


                            decimal d = 0;
                            Ledger objLedger = new Ledger();
                            objLedger.VoucherId = item.VoucherId;
                            objLedger.VoucherType = item.VoucherType;
                            objLedger.AccountNo = item.SourceCOAId.ToString();
                            objLedger.LedgerId = objLedger.GetMaxLedgerId() + 1;
                            objLedger.TransactionNo = item.VoucherNo;
                            objLedger.TransactionDate = Convert.ToDateTime(ledgerPostingDatePicker.SelectedDate);
                            //  objLedger.TransactionDate = item.VoucherDate;
                            objLedger.LastUpdateTime = DateTime.Now;
                            objLedger.UserId = item.UpdateBy;
                            objLedger.Description = item.VoucherDescription;
                            objLedger.Debit = Convert.ToDecimal(item.VoucherTotal);
                            objLedger.Credit = d;
                            objLedger.ContraAccountNo = Convert.ToString(7);
                            objLedger.CompanyId = _company.CompanyId;
                            objLedger.InsertLedger();
                            objLedger = new Ledger();
                            objLedger.VoucherId = item.VoucherId;
                            objLedger.VoucherType = item.VoucherType;
                            objLedger.AccountNo = Convert.ToString(7);
                            objLedger.LedgerId = objLedger.GetMaxLedgerId() + 1;
                            objLedger.TransactionNo = item.VoucherNo;
                            objLedger.TransactionDate = Convert.ToDateTime(ledgerPostingDatePicker.SelectedDate);
                            objLedger.LastUpdateTime = DateTime.Now;
                            objLedger.UserId = item.UpdateBy;
                            objLedger.Description = item.VoucherDescription;
                            objLedger.Debit = d;
                            objLedger.Credit = Convert.ToDecimal(item.VoucherTotal);
                            objLedger.ContraAccountNo = item.SourceCOAId.ToString();
                            objLedger.CompanyId = _company.CompanyId;
                            objLedger.InsertLedger();
                            TempLedgerReportTable newTempLedger = new TempLedgerReportTable();
                            newTempLedger.VoucherTrasactionDate = item.VoucherDate;
                            newTempLedger.MasterAccountCOAid = 7;
                            var temp1 = temp11.GetChartOfAccountByCoaId(newTempLedger.MasterAccountCOAid,
                                _company.CompanyId);
                            newTempLedger.MasterAccount = temp1.CoaTitle;
                            newTempLedger.VoucherType = "Expense";
                            newTempLedger.TransactionCode = item.VoucherNo;
                            newTempLedger.Debit = 0;
                            newTempLedger.Credit = Convert.ToDecimal(item.VoucherTotal);


                            newTempLedger.ContraAccountId = item.SourceCOAId;
                            var temp2 = temp11.GetChartOfAccountByCoaId(item.SourceCOAId, _company.CompanyId);
                            newTempLedger.ContraAccount = temp2.CoaTitle;
                            if (newTempLedger.GetAllTempLedgerReportTable().Count > 0 && (
                                newTempLedger.GetAllTempLedgerReportTable()
                                    .Any(pp => pp.MasterAccountCOAid == item.SourceCOAId)))
                            {
                                int id = new TempLedgerReportTable().GetMaxTempLedgerIdByMasterAccountId(7);
                                var item22 = new TempLedgerReportTable().GetTempLedgerReportTableByVoucherTempId(id);
                                newTempLedger.OpeningBalance = item22.ClosingBalance;
                                newTempLedger.ClosingBalance = (newTempLedger.OpeningBalance - newTempLedger.Credit);
                                newTempLedger.VoucherTempId =
                                    new TempLedgerReportTable().GetMaximumTempLedgerReportTableId() + 1;
                                newTempLedger.InsertTempLedgerReportTable();

                            }
                            else
                            {

                                newTempLedger.ClosingBalance = -newTempLedger.Credit;
                                newTempLedger.VoucherTempId =
                                    new TempLedgerReportTable().GetMaximumTempLedgerReportTableId() + 1;
                                newTempLedger.InsertTempLedgerReportTable();
                            }






                            //objVoucherMaster.VoucherId = item.VoucherId;
                            //objVoucherMaster.IsPosted = true;
                            //objVoucherMaster.InsertVoucherMaster();

                            item.IsPosted = true;
                            item.UpdateVoucherMaster();
                        }
                    }

                }


                Alert.Show("Data Posted Successfully to Ledger");

            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }
        }


    }
}