﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Balancika.Bll;
using Balancika.BLL;
using BALANCIKA.BLL;
using Telerik.Web.UI;

namespace Balancika.WebApplication
{
    public partial class VoucherInformation : System.Web.UI.Page
    {
        private bool isNewEntry;
        private Users user;
        private Company _company = new Company();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["customVoucherMessage"] != null)
            {
                string msg = (string)Session["customVoucherMessage"];
                Alert.Show(msg);
                Session["customVoucherMessage"] = null;
            }
            _company = (Company)Session["Company"];
            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx");
                }
                else
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx?" + str);
                }

            }
            if (!IsPostBack)
            {
                this.LoadFirstTime();

            }


        }

        private void LoadFirstTime()
        {
            this.LoadChatOfAccountDropDownList();
            this.LoadCostCenterDropDownList();
            this.LoadPaymentModeDropDownList();
            this.LoadSourceOfAccount();
            Session["VoucherMasterInformation"] = null;
            Session["VoucherDetails"] = null;
            //divVoucherMasterInformation.Visible = true;
            //divVoucherDetailsInformation.Visible = false;
            divChequeDetails.Visible = false;
            divUpdateButton.Visible = false;
            divButtonForSingleDetailsSave.Visible = true;
            divChequeSaveButton.Visible = false;
            btnUpdateChequeDiv.Visible = false;
            loadRadGrid();
            loadBankDropdownList();
            //loadChequeDetailsGrid();
            loadChequeDropdown();

        }

        private void LoadChatOfAccountDropDownList()
        {
            List<ChartOfAccount> coaList = new ChartOfAccount().GetAllChartOfAccount(_company.CompanyId);
            coaDropDownList.DataSource = coaList;
            coaDropDownList.DataTextField = "CoaTitle";
            coaDropDownList.DataValueField = "CoaId";
            coaDropDownList.DataBind();
        }

        private void LoadCostCenterDropDownList()
        {
            List<CostCenter> costCenterList = new CostCenter().GetAllCostCenter(_company.CompanyId);
            costCenterDropDownlist.DataSource = costCenterList;
            costCenterDropDownlist.DataTextField = "CostCenterName";
            costCenterDropDownlist.DataValueField = "CostCenterId";
            costCenterDropDownlist.DataBind();
        }

        private void LoadPaymentModeDropDownList()
        {

            //List<ChartOfAccountGroup> coaGroup=new ChartOfAccountGroup().GetAllChartOfAccountGroup(_company.CompanyId);
            //ddlPaymentMode.DataSource =coaGroup;
            //ddlPaymentMode.DataTextField = "CoaGroupName";
            //ddlPaymentMode.DataValueField = "CoaGroupId";
            //ddlPaymentMode.DataBind();
            List<string> listItem = new List<string>();
            listItem.Add("Cash");
            listItem.Add("Bank");
            ddlPaymentMode.DataSource = listItem;
            ddlPaymentMode.DataBind();
        }

        private void loadBankDropdownList()
        {

            List<Bank> objBankAccounts = new Bank().GetAllBank(_company.CompanyId);
            ddlBankAccountList.DataSource = objBankAccounts;
            ddlBankAccountList.DataTextField = "BankName";
            ddlBankAccountList.DataValueField = "BankId";
            ddlBankAccountList.DataBind();
        }


        //me
        //private void loadChequeDetailsGrid()
        //{
        //    VoucherChequeInfo objChequeInfo = new VoucherChequeInfo();

        //    chequeDetailsGrid.DataSource = objChequeInfo.GetAllVoucherChequeInfo();
        //    chequeDetailsGrid.DataBind();
        //}
        //me

        private void loadChequeDropdown()
        {
           

            ddlChequeType.Items.Add("Account Payee");
            ddlChequeType.Items.Add("Bearer");
        }

        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;

            }
            user = (Users)Session["user"];
            return user.UserId != 0;

        }

        private void ClearMaster()
        {
            costCenterDropDownlist.SelectedIndex =
                coaDropDownList.SelectedIndex = ddlPaymentMode.SelectedIndex = -1;
            voucherDatePicker.SelectedDate = null;
            txtVoucherTotal.Value =
                txtVoucherDescription.Value =
                        txtPaymentRef.Value = "";


        }

        protected void btnSaveVoucherMasterInfomation_Click(object sender, EventArgs e)
        {
            saveVoucherMasterInformation(1);
        }

        private void saveVoucherMasterInformation(int bit)
        {
            try
            {
                VoucherMaster objVoucherMaster = new VoucherMaster();
                objVoucherMaster.VoucherId = new VoucherMaster().GetMaxVoucherMasterId() + 1;

                objVoucherMaster.VoucherDate = voucherDatePicker.SelectedDate.ToString();
                objVoucherMaster.VoucherType = "Receipt";
                objVoucherMaster.CompanyId = _company.CompanyId;

                objVoucherMaster.CostCenterId = int.Parse(costCenterDropDownlist.SelectedIndex == -1 ? "0" : costCenterDropDownlist.SelectedItem.Value);
                objVoucherMaster.PaymentMode = ddlPaymentMode.SelectedText; //paymentModeDropDownList.SelectedText.ToString();
                objVoucherMaster.VoucherDescription = txtVoucherDescription.Value;

                objVoucherMaster.UpdateBy = user.UserId;
                objVoucherMaster.UpdateDate = DateTime.Now;
                objVoucherMaster.ApprovedBy = user.UserId;
                objVoucherMaster.ApprovedDate = DateTime.Now;
                objVoucherMaster.PostDate = DateTime.Now;
                objVoucherMaster.VoucherTotal = txtVoucherTotal.Value;
                objVoucherMaster.PaymentRef = txtPaymentRef.Value;
                objVoucherMaster.IsPosted = false;
                objVoucherMaster.Status = "Checked";
                objVoucherMaster.SourceCOAId = int.Parse(ddlSourceOfAccount.SelectedItem.Value);
               
                

                if (bit == 1)
                {
                    objVoucherMaster.SourceOfAccount = "Voucher";
                    
                    VoucherDetailTemp objVoucherDetailTemp = new VoucherDetailTemp();
                    List<VoucherDetailTemp> objTempVoucherList =
                        objVoucherDetailTemp.GetAllVoucherDetailTempByUserId(user.UserId);

                    objVoucherMaster.VoucherTotal = objTempVoucherList.Sum(d => d.Amount).ToString();
                    objVoucherMaster.InsertVoucherMaster();
                    Decimal tmpAmount = 0;
                    foreach (var x in objTempVoucherList)
                    {
                        VoucherDetail objVoucherDetail = new VoucherDetail();
                        objVoucherDetail.VoucherDetailId = objVoucherDetail.GetMaxVoucherDetailsId() + 1;
                        objVoucherDetail.COAId = x.COAId;
                        objVoucherDetail.LineDesciption = x.LineDesciption;
                        tmpAmount += objVoucherDetail.Amount = x.Amount;
                        objVoucherDetail.VoucherId = objVoucherMaster.GetMaxVoucherMasterId();
                        objVoucherDetail.InsertVoucherDetail();
                        x.DeleteVoucherDetailTempById(x.Id);


                    }
                    // objVoucherMaster.VoucherTotal = tmpAmount.ToString();

                    Session["customVoucherMessage"] = "Voucher Information Saved Successfully with Voucher Details";
                    Response.Redirect("VoucherInformation.aspx");
                }
                else
                {
                    objVoucherMaster.SourceOfAccount = "Cheque";
                    objVoucherMaster.VoucherTotal = txtChequeAmount.Value;
                    objVoucherMaster.InsertVoucherMaster();
                    VoucherChequeInfo objVoucherCheque=new VoucherChequeInfo();
                    objVoucherCheque.ChequeId = objVoucherCheque.GetMaxVoucherChequeInfoId() + 1;
                    objVoucherCheque.VoucherType = "Receipt";
                    objVoucherCheque.VoucherId = objVoucherMaster.VoucherId;
                    objVoucherCheque.BankId = int.Parse(ddlBankAccountList.SelectedItem.Value);
                    objVoucherCheque.BankName = ddlBankAccountList.SelectedItem.Text;
                    objVoucherCheque.Branch = txtBranchName.Value;
                    objVoucherCheque.AccountNo = txtBankAccountNumber.Value;
                    objVoucherCheque.ChequeNo = txtChequeNumber.Value;
                    objVoucherCheque.ChequeType = ddlChequeType.SelectedItem.Text;
                    objVoucherCheque.ChequeAmount = Decimal.Parse(txtChequeAmount.Value);
                    objVoucherCheque.IsPostToLedger = false;
                    objVoucherCheque.SubmiteDate = chequeSubmitDate.SelectedDate.Value.ToShortDateString();
                    objVoucherCheque.SubmiteToBank = txtSubmitBank.Checked==true?1:0;
                    objVoucherCheque.InsertVoucherChequeInfo();
                    ClearChequeInformation();
                   // loadChequeDetailsGrid(); 

                    Session["customVoucherMessage"] = "Voucher Information Saved Successfully with Cheque Information";
                    Response.Redirect("VoucherInformation.aspx");
                }


                //Session["VoucherMasterInformation"] = objVoucherMaster;
                divVoucherMasterInformation.Visible = true;
                //divButtonForMasterSave.Visible = false;
                divChequeMasterSaveButton.Visible = false;
                divButtonForSingleDetailsSave.Visible = true;
                //divChequeSaveButton.Visible = true;

                //voucherDetailsGrid.DataSource = objVoucherMaster.GetAllVoucherMaster(_company.CompanyId);
                //voucherDetailsGrid.DataBind();
                loadRadGrid();

                lblVoucherno.Text = objVoucherMaster.VoucherNo;
                ClearMaster();
                
            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }
        }

        public void ClearChequeInformation()
        {
            try
            {
                ddlBankAccountList.SelectedIndex = -1;
                txtBranchName.Value =
                    txtBankAccountNumber.Value =
                        txtChequeNumber.Value = txtChequeAmount.Value = "";
                //txtIsPostToLedger.Checked = false;




            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }
        }
        protected void btnVoucherMasterInfomationClear_Click(object sender, EventArgs e)
        {
            this.ClearMaster();
        }

        public void ClearDetailVoucher()
        {

            txtVoucherAmount.Value = txtVoucherDescription.Value = txtCOANO.Value = "";
            coaDropDownList.SelectedIndex = -1;


        }

        protected void btnSaveVoucherSingleDetailInformation_Click(object sender, EventArgs e)
        {
            //try
            //{

            //    VoucherMaster tmpVoucherMaster = (VoucherMaster)Session["VoucherMasterInformation"];
            //    VoucherDetail objVoucherDetail = new VoucherDetail();
            //    if (Session["VoucherDetails"] == null)
            //    {
            //        objVoucherDetail.VoucherDetailId = new VoucherDetail().GetMaxVoucherDetailsId() + 1;

            //    }
            //    objVoucherDetail.COAId = int.Parse(coaDropDownList.SelectedItem.Value);//tmpVoucherMaster.SourceCOAId;
            //    objVoucherDetail.VoucherId = new VoucherMaster().GetMaxVoucherMasterId();
            //    objVoucherDetail.LineDesciption = txtVoucherDetailsDescription.Value;
            //    objVoucherDetail.Amount = Convert.ToDecimal(txtVoucherAmount.Value);
            //    objVoucherDetail.InsertVoucherDetail();

            //    loadRadGrid();
            //}
            //catch (Exception ex)
            //{
            //    Alert.Show(ex.Message);
            //}
            try
            {

                VoucherDetailTemp objVoucherDetail = new VoucherDetailTemp();
                objVoucherDetail.COAId = int.Parse(coaDropDownList.SelectedItem.Value);
                objVoucherDetail.LineDesciption = txtVoucherDetailsDescription.Value;
                objVoucherDetail.Amount = Convert.ToDecimal(txtVoucherAmount.Value);
                objVoucherDetail.userId = user.UserId;
                objVoucherDetail.Id = objVoucherDetail.GetMaxVoucherDetailTempId() + 1;
                objVoucherDetail.InsertVoucherDetailTemp();
                loadRadGrid();

            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }
        }
        protected void btnVoucherSingleDetailInformationClear_Click(object sender, EventArgs e)
        {
            txtVoucherAmount.Value = txtVoucherAmount.Value = "";

        }

        protected void btnVoucherDetailsAllInformationClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("VoucherInformation.aspx", true);
        }

        //Last Worked Here...................
        private void loadRadGrid()
        {
            VoucherDetailTemp objVoucherDetail = new VoucherDetailTemp();
            voucherDetailsGrid.DataSource = objVoucherDetail.GetAllVoucherDetailTemp().Where(d => d.userId == user.UserId).ToList();
            voucherDetailsGrid.DataBind();
        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

           

        }

        private void LoadSourceOfAccount()
        {
            List<ChartOfAccount> coAccounts=new ChartOfAccount().GetAllChartOfAccount(_company.CompanyId);
            ddlSourceOfAccount.DataSource = coAccounts;
            ddlSourceOfAccount.DataTextField = "CoaTitle";
            ddlSourceOfAccount.DataValueField = "CoaId";
            ddlSourceOfAccount.DataBind();

        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                if (CheckBox1.Checked&&ddlPaymentMode.SelectedText=="Bank")
                {
                    divChequeDetails.Visible = true;
                    divVoucherDetailsInformation.Visible = false;
                }
                else
                {
                    divChequeDetails.Visible = false;
                    divVoucherDetailsInformation.Visible = true;
                    CheckBox1.Checked = false;
                    Alert.Show("Please select Bank as Payment Mode");


                }
            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }
        }

        protected void voucherDetailsGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string id = string.Empty;
            if (e.CommandName == "btnEdit" || e.CommandName == "btnDelete")
            {
                GridDataItem item = (GridDataItem)e.Item;
                id = item["colId"].Text;
            }

            switch (e.CommandName)
            {
                case "btnEdit":

                    VoucherDetailTemp objVoucherDetail = new VoucherDetailTemp();
                    var x = objVoucherDetail.GetVoucherDetailTempById(int.Parse(id));
                    coaDropDownList.SelectedIndex = x.COAId;
                    txtVoucherDetailsDescription.Value = x.LineDesciption;
                    txtVoucherAmount.Value = x.Amount.ToString();
                    Label1.Text = x.Id.ToString();
                    divButtonForSingleDetailsSave.Visible = false;
                    divUpdateButton.Visible = true;

                    break;
                case "btnDelete":
                    int delete = new VoucherDetailTemp().DeleteVoucherDetailTempById(int.Parse(id));
                    if (delete == 0)
                    {
                        Alert.Show("Data is not deleted...!");
                    }
                    else
                    {
                        voucherDetailsGrid.DataSource = new VoucherDetail().GetAllVoucherDetail();
                        voucherDetailsGrid.DataBind();
                    }
                    break;
                default:
                    break;
            }
        }

       

        protected void divUpdateVoucherDetails_Click(object sender, EventArgs e)
        {
            VoucherDetailTemp objVoucherDetail = new VoucherDetailTemp();

            try
            {
                objVoucherDetail.COAId = int.Parse(coaDropDownList.SelectedItem.Value);

                objVoucherDetail.LineDesciption = txtVoucherDetailsDescription.Value;
                objVoucherDetail.Amount = Decimal.Parse(txtVoucherAmount.Value);
                objVoucherDetail.Id = int.Parse(Label1.Text);
                objVoucherDetail.userId = user.UserId;
                objVoucherDetail.UpdateVoucherDetailTemp();
                divButtonForSingleDetailsSave.Visible = true;
                divUpdateButton.Visible = false;
                loadRadGrid();
            }
            catch (Exception ex)
            {

                Alert.Show(ex.Message);
            }
        }

        //Cheque Part..............................................
        protected void btnCheckDetailsSave_Click(object sender, EventArgs e)
        {
            VoucherChequeInfo objVoucherCheque = new VoucherChequeInfo();

            try
            {
                objVoucherCheque.ChequeId = objVoucherCheque.GetMaxVoucherChequeInfoId() + 1;
                objVoucherCheque.VoucherType = "Receipt";
                objVoucherCheque.VoucherId = new VoucherMaster().GetMaxVoucherMasterId();
                objVoucherCheque.BankId = int.Parse(ddlBankAccountList.SelectedItem.Value);
                objVoucherCheque.BankName = ddlBankAccountList.SelectedItem.Value;
                objVoucherCheque.Branch = txtBranchName.Value;
                objVoucherCheque.AccountNo = txtBankAccountNumber.Value;
                objVoucherCheque.ChequeNo = txtChequeNumber.Value;
                objVoucherCheque.ChequeType =ddlChequeType.SelectedItem.Text;
                objVoucherCheque.ChequeAmount = Decimal.Parse(txtChequeAmount.Value);
                objVoucherCheque.IsPostToLedger = false;
                objVoucherCheque.SubmiteDate = chequeSubmitDate.SelectedDate.Value.ToShortDateString();
                objVoucherCheque.SubmiteToBank = 1;
                objVoucherCheque.InsertVoucherChequeInfo();
                //loadChequeDetailsGrid();
            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }
        }
        

        //protected void chequeDetailsGrid_ItemCommand(object sender, GridCommandEventArgs e)
        //{
        //    string id = string.Empty;
        //    if (e.CommandName == "btnEdit" || e.CommandName == "btnDelete")
        //    {
        //        GridDataItem item = (GridDataItem)e.Item;

        //        id = item["colId"].Text;

        //    }

        //    switch (e.CommandName)
        //    {
        //        case "btnEdit":
        //            VoucherChequeInfo objVoucherCheque = new VoucherChequeInfo();
        //            var x = objVoucherCheque.GetVoucherChequeInfoByChequeId(int.Parse(id));
        //            ddlBankAccountList.SelectedValue = x.BankName;
        //            txtBranchName.Value = x.Branch;
        //            txtBankAccountNumber.Value = x.AccountNo;
        //            txtChequeNumber.Value = x.ChequeNo;
        //            txtChequeAmount.Value = x.ChequeAmount.ToString();
        //            txtIsPostToLedger.Checked = x.IsPostToLedger;
        //            chequeSubmitDate.SelectedDate = Convert.ToDateTime(x.SubmiteDate);
        //            lblChequeDetailsIdHidden.Text = x.ChequeId.ToString();
        //            lblId1.Text = x.VoucherId.ToString();
        //            btnCheckDetailsSave.Visible = false;
        //            btnUpdateChequeDiv.Visible = true;
        //            divChequeMasterSaveButton.Visible = false;
        //            break;
        //        case "btnDelete":
        //            VoucherChequeInfo objChequeInfo= new VoucherChequeInfo().GetVoucherChequeInfoByChequeId(int.Parse(id));
        //            int delete1 = new VoucherMaster().DeleteVoucherMasterByVoucherId(objChequeInfo.VoucherId);
        //            int delete = new VoucherChequeInfo().DeleteVoucherChequeInfoByChequeId(int.Parse(id));
        //            if (delete == 0&&delete1==0)
        //            {
        //                Alert.Show("Data is not deleted...!");
        //            }
        //            else
        //            {
        //                chequeDetailsGrid.DataSource = new VoucherChequeInfo().GetAllVoucherChequeInfo();
        //                chequeDetailsGrid.DataBind();
        //            }
        //            break;
        //        default:
        //            break;
        //    }
        //}
        protected void btnChequeMasterSave_Click(object sender, EventArgs e)
        {
            saveVoucherMasterInformation(2);
        }

        //protected void btnUpdateCheque_Click(object sender, EventArgs e)
        //{

        //    VoucherChequeInfo objVoucherCheque = new VoucherChequeInfo();

        //    try
        //    {
        //        objVoucherCheque.ChequeId = Convert.ToInt64(lblChequeDetailsIdHidden.Text);
        //        objVoucherCheque.VoucherType = "Recieve Voucher";
        //        objVoucherCheque.VoucherId = int.Parse(lblId1.Text);
        //        objVoucherCheque.BankId = int.Parse(ddlBankAccountList.SelectedItem.Value);
        //        objVoucherCheque.BankName = ddlBankAccountList.SelectedItem.Value;
        //        objVoucherCheque.Branch = txtBranchName.Value;
        //        objVoucherCheque.AccountNo = txtBankAccountNumber.Value;
        //        objVoucherCheque.ChequeNo = txtChequeNumber.Value;
        //        objVoucherCheque.ChequeType = ddlChequeType.SelectedValue;
        //        objVoucherCheque.ChequeAmount = Decimal.Parse(txtChequeAmount.Value);
        //        objVoucherCheque.IsPostToLedger = txtIsPostToLedger.Checked;
        //        objVoucherCheque.SubmiteDate = chequeSubmitDate.SelectedDate.Value.ToShortDateString();
        //        objVoucherCheque.SubmiteToBank = 1;
        //        objVoucherCheque.UpdateVoucherChequeInfo();
        //        loadChequeDetailsGrid();
        //        btnUpdateChequeDiv.Visible = false;
        //        divChequeMasterSaveButton.Visible = true;
        //        ClearMaster();
        //        ClearChequeInformation();
        //    }
        //    catch (Exception ex)
        //    {
        //        Alert.Show(ex.Message);
        //    }
        //}

        //protected void chequeDetailsGrid_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        //{
        //    loadChequeDetailsGrid();
        //}

        //protected void chequeDetailsGrid_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        //{
        //    loadChequeDetailsGrid();
        //}

        protected void voucherDetailsGrid_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            loadRadGrid();
        }

        protected void voucherDetailsGrid_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            loadRadGrid();
        }


    }
}
