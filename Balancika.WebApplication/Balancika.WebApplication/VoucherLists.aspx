﻿

<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="VoucherLists.aspx.cs" Inherits="Balancika.WebApplication.VoucherLists" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <form id="form1" runat="server">
     <asp:ScriptManager runat="server">
                </asp:ScriptManager>
    <section class="form-horizontal">

        <div class="box">
            <div id="VoucherList" runat="server" class="box box-primary" style="width: auto">
            </div>
            <div id="header" runat="server" class="box-header with-border">
                <h1 style="font-size: 20px; font-weight: bold; color: green"><b>Voucher</b>List</h1>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                    <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True" GroupPanelPosition="Top" AutoGenerateColumns="False" CellSpacing="-1" AllowMultiRowSelection="true" ClientSettings-Selecting-AllowRowSelect="true">
                        <MasterTableView>
                            <Columns>
                                <telerik:GridClientSelectColumn FilterControlAltText="Filter ClientSelectColumn column" UniqueName="ClientSelectColumn"></telerik:GridClientSelectColumn>
                                <telerik:GridBoundColumn DataField="VoucherDate" HeaderText="Voucher Date" UniqueName="colVoucherDate" FilterControlAltText="Filter colVoucherDate column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="VoucherNo" HeaderText="Voucher Number" UniqueName="colVoucherNo" FilterControlAltText="Filter colVoucherNo column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="VoucherType" HeaderText="Voucher Type" UniqueName="colVoucherType" FilterControlAltText="Filter colVoucherType column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PaymentMode" HeaderText="Payment Mode" UniqueName="colPaymentMode" FilterControlAltText="Filter colPaymentMode column" AllowSorting="true"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="VoucherDescription" HeaderText="Description" UniqueName="colVoucherDescription" FilterControlAltText="Filter colVoucherDescription column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="VoucherTotal" HeaderText="Voucher Total" UniqueName="colVoucherTotal" FilterControlAltText="Filter colVoucherTotal column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PaymentRef" HeaderText="Payment Reference" UniqueName="colPaymentRef" FilterControlAltText="Filter colPaymentRef column"></telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="IsPosted" HeaderText="Post to Ledger" UniqueName="colPosted" DataType="System.Boolean" FilterControlAltText="Filter colPosted column"></telerik:GridCheckBoxColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>                        
                    </div>
                </div>
                
                <div class="row box-body">
                    <div class="col-md-6">
                        <label for="LedgerPostingDatge" class="control-label">Ledger Posting Date</label>
                        <telerik:RadDatePicker ID="ledgerPostingDatePicker" runat="server"></telerik:RadDatePicker>
                    </div>
                    <div class="col-md-6">
                        <asp:Button ID="btnLEdgerPosting" runat="server" Text="Post All TO Ledger" CssClass="btn btn-success" OnClick="btnLEdgerPosting_Click"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
       </form>
</asp:Content>
