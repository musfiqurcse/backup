﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="VoucherPayment.aspx.cs" Inherits="Balancika.WebApplication.VoucherPayment" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2015.1.225.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainHeader" runat="server">
    <h1 id="txtVoucherTypeName" runat="server">Recieve Voucher Information </h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <asp:Label ID="lblId1" runat="server" Visible="False" Text=""></asp:Label>
      <asp:Label ID="lblId2" runat="server" Visible="False" Text=""></asp:Label>
      <asp:Label ID="lblId3" runat="server" Visible="False" Text=""></asp:Label>
    <form id="form1" runat="server">
        <section class="form-horizontal">
            <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>

            <div class="box">
                <div id="divVoucherMasterInformation" runat="server" class="box box-primary" style="width: auto">

                    <div class="box-header with-border" style="width: 100%">
                        <h1 style="font-size: 20px; font-weight: bold; color: green">Add New <b>Voucher</b> Information </h1>
                    </div>
                    <div id="divVoucherMasterInformationBody" class="box-body" runat="server">
                        <div class="col-sm-6">
                            <div class="form-group ">
                                <label for="voucherDatePicker" class="col-sm-4 control-label">Voucher Date</label>
                                <div class="col-xs-8">
                                    <telerik:RadDatePicker Skin="Bootstrap" ID="voucherDatePicker" runat="server" Width="100%" SelectedDate='<%# System.DateTime.Today %>'></telerik:RadDatePicker>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ">
                                <label for="paymentModeDropDownList" class="col-sm-4 control-label">Payment Mode</label>
                                <div class="col-xs-8">
                                   
                                     <telerik:RadDropDownList ID="ddlPaymentMode"
                                            Skin="Bootstrap"
                                            runat="server" padding-left="20px"
                                            Width="100%"
                                            AutoPostBack="true"
                                            DefaultMessage="Select Payment Mode"
                                            OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                        </telerik:RadDropDownList>
                                    

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="txtVoucherDescription" class="col-sm-4 control-label">Voucher Description</label>
                                <div class="col-sm-8">
                                    <textarea type="text" class="form-control" width="60px" name="txtVoucherDescription" id="txtVoucherDescription" placeholder="Description" runat="server"></textarea>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ">
                                <label for="costCenterDropDownlist" class="col-sm-4 control-label">Cost Center</label>
                                <div class="col-xs-8">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server">
                                        <telerik:RadDropDownList ID="costCenterDropDownlist"
                                            Skin="Bootstrap"
                                            runat="server" padding-left="20px"
                                            Width="100%"
                                            AutoPostBack="true"
                                            DefaultMessage="Select Cost Center"
                                            >
                                        </telerik:RadDropDownList>
                                    </telerik:RadAjaxPanel>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="txtVoucherTotal" class="col-sm-4 control-label">Voucher Total</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" width="60px" name="txtVoucherTotal" id="txtVoucherTotal" placeholder="Voucher Total" runat="server" clientidmode="Static" />
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="txtPaymentRef" class="col-sm-4 control-label">Payment Reference</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" width="60px" name="txtPaymentRef" id="txtPaymentRef" placeholder="Payment Reference" runat="server" clientidmode="Static" />
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="txtPaymentRef" class="col-sm-4 control-label">Source Of Account</label>
                                <div class="col-lg-8">
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel4" runat="server">
                                        <telerik:RadDropDownList ID="ddlSourceOfAccount"
                                            Skin="Bootstrap"
                                            runat="server" padding-left="20px"
                                            Width="100%"
                                            AutoPostBack="true"
                                            DefaultMessage="Select Source Of Account">
                                        </telerik:RadDropDownList>
                                    </telerik:RadAjaxPanel>

                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="chkCheque" class="col-sm-4 control-label">Cheque Details</label>
                                <div class="col-lg-8">
                                    <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged" />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <%-- Introducing Cheque Details --%>

                <div id="divChequeDetails" runat="server" class="box box-primary" style="width: auto">
                    <div class="box-header with-border" style="width: 100%">
                        <h1 style="font-size: 20px; font-weight: bold; color: green">Add  <b>Cheque</b> Details Information </h1>
                    </div>
                    <%--<div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtVoucherType" class="col-sm-4 control-label">Voucher Type: </label>
                            <div class="col-lg-8">
                                <asp:DropDownList ID="ddlVoucherType" runat="server" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                            </div>

                        </div>
                    </div>--%>


                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtVoucherName" class="col-sm-4 control-label">Bank Name</label>
                            <div class="col-lg-8">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                                    <telerik:RadDropDownList ID="ddlBankAccountList" runat="server"
                                        AutoPostBack="true"
                                        Skin="Bootstrap"
                                        Width="100%"
                                        padding-left="20px">
                                    </telerik:RadDropDownList>
                                </telerik:RadAjaxPanel>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtBranchName" class="col-sm-4 control-label">Branch Name</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtBranchName" id="txtBranchName" placeholder="Enter Branch Name" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtBankAccountNumber" class="col-sm-4 control-label">Account Number</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtBankAccountNumber" id="txtBankAccountNumber" placeholder="Enter Account Number" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtChequeNumber" class="col-sm-4 control-label">Cheque No</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtChequeNumber" id="txtChequeNumber" placeholder="Enter Cheque Number" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtChequeType" class="col-sm-4 control-label">Cheque Type</label>
                            <div class="col-lg-8">
                                <asp:DropDownList ID="ddlChequeType" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtChequeAmount" class="col-sm-4 control-label">Cheque Amount</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtChequeAmount" id="txtChequeAmount" placeholder="Enter Account Number" runat="server" clientidmode="Static" />
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtSubmitDate" class="col-sm-4 control-label">Submit Date</label>
                            <div class="col-lg-8">
                                <telerik:RadDatePicker Skin="Bootstrap" ID="chequeSubmitDate" runat="server" Width="100%" SelectedDate='<%# System.DateTime.Today %>'></telerik:RadDatePicker>
                            </div>

                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtIsPostToLedger" class="col-sm-4 control-label">Submit To Bank</label>
                            <div class="col-lg-8">
                                <input type="checkbox" width="60px" name="txtSubmitBank" id="txtSubmitBank"  runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>
                   <%--  <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtIsPostToLedger" class="col-sm-4 control-label">Post to Ledger</label>
                            <div class="col-lg-8">
                                <input type="checkbox" width="60px" name="txtIsPostToLedger" id="txtIsPostToLedger" placeholder="Enter Account Number" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>--%>
                    <div class="form-group" id="divChequeSaveButton" runat="server">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="btnCheckDetailsSave" runat="server" ClientIDMode="Static" CssClass="btn btn-success" Text="Save Cheque Details" OnClick="btnCheckDetailsSave_Click" />

                            <input class="btn btn-warning" runat="server" type="button" value="Clear Information" />
                        </div>
                    </div>
                    <div class="form-group" id="divChequeMasterSaveButton" runat="server">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="btnChequeMasterSave" runat="server" ClientIDMode="Static" CssClass="btn btn-success" Text="Save Voucher Master Information" OnClick="btnChequeMasterSave_Click" />

                            <input class="btn btn-warning" runat="server" onserverclick="btnVoucherMasterInfomationClear_Click" type="button" value="Clear Master Information" />
                        </div>
                    </div>
                    <div class="form-group" id="btnUpdateChequeDiv" runat="server">
                        <div class="col-sm-offset-2 col-sm-10">
                            <%--<asp:Button ID="btnUpdateCheque" runat="server" ClientIDMode="Static" CssClass="btn btn-success" Text="Update Cheque Details" OnClick="btnUpdateCheque_Click" />--%>

                            <input class="btn btn-warning" runat="server" onserverclick="btnVoucherSingleDetailInformationClear_Click" type="button" value="Clear Information" />
                        </div>
                    </div>
                    <asp:Label ID="lblChequeDetailsIdHidden" runat="server" Text="Label" Visible="false"></asp:Label>
                 
                </div>


                <%-- Details Part For Vouchar MASTER --%>

                <div id="divVoucherDetailsInformation" runat="server" class="box box-primary" style="width: auto">
                    <div class="box-header with-border" style="width: 100%">
                        <h1 style="font-size: 20px; font-weight: bold; color: green">Add  <b>Voucher</b> Details Information </h1>
                        <asp:Label Text="fff" runat="server" ID="lblVoucherno" Visible="false" />
                    </div>


                    <div class="col-sm-6">
                        <div class="form-group ">
                            <label for="coaDropDownList" class="col-sm-4 control-label">Chart of Account</label>
                            <div class="col-xs-8">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel3" runat="server">
                                    <telerik:RadDropDownList ID="coaDropDownList"
                                        Skin="Bootstrap"
                                        runat="server" padding-left="20px"
                                        Width="100%"
                                        AutoPostBack="true"
                                        DefaultMessage="Select Chart of Account">
                                    </telerik:RadDropDownList>

                                </telerik:RadAjaxPanel>
                            </div>

                        </div>
                    </div>
                    <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtCOANO" class="col-sm-4 control-label">Chart of Account </label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtCOANO" id="txtCOANO" placeholder="Chart Of Accounting Number" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="txtVoucherDetailsDescription" class="col-sm-4 control-label">Description</label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control" width="60px" name="txtVoucherDetailsDescription" id="txtVoucherDetailsDescription" placeholder="Description" runat="server"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtVoucherAmount" class="col-sm-4 control-label">Amount</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtVoucherAmount" id="txtVoucherAmount" placeholder="Amount" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>
                    

                    <div class="form-group" id="divButtonForSingleDetailsSave" runat="server">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="btnSaveVoucherDetailsInformation" runat="server" ClientIDMode="Static" CssClass="btn btn-success" Text="Add Voucher Details" OnClick="btnSaveVoucherSingleDetailInformation_Click" />

                            <input class="btn btn-warning" runat="server" onserverclick="btnVoucherSingleDetailInformationClear_Click" type="button" value="Clear Information" />
                        </div>
                    </div>

                    <div class="form-group" id="divUpdateButton" runat="server">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="divUpdateVoucherDetails" runat="server" ClientIDMode="Static" CssClass="btn btn-success" Text="Update Detials" OnClick="divUpdateVoucherDetails_Click" />

                            <input class="btn btn-warning" runat="server" onserverclick="btnVoucherSingleDetailInformationClear_Click" type="button" value="Clear Information" />
                        </div>
                    </div>

                    <%-- Grid For Showing Details Started --%>


                    <div class="box">

                        <div class="box-body">
                            <div id="divVoucherDetailsInformationTable" class="form-inline">
                                <div class="row">
                                    <div class="col-sm-6"></div>
                                    <div class="col-sm-6"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <telerik:RadGrid ID="voucherDetailsGrid"
                                            runat="server"
                                            CellSpacing="-1"
                                            GridLines="Both"
                                            GroupPanelPosition="Top"
                                            AllowPaging="True"
                                            Skin="Bootstrap"
                                            AutoGenerateColumns="False"
                                            CssClass="" OnItemCommand="voucherDetailsGrid_ItemCommand" OnPageIndexChanged="voucherDetailsGrid_PageIndexChanged" OnPageSizeChanged="voucherDetailsGrid_PageSizeChanged">
                                            <MasterTableView ShowFooter="true">
                                                <Columns>
                                                    <telerik:GridBoundColumn DataField="Id" HeaderText="ID" UniqueName="colId" Display="False">
                                                    </telerik:GridBoundColumn>
                                                    
                                                    <telerik:GridBoundColumn DataField="COAId" FilterControlAltText="Filter colCOAId column" HeaderText="COA Id" UniqueName="colCOAId">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="LineDesciption" FilterControlAltText="Filter colDescription column" HeaderText="Line Description" UniqueName="colDescription" FooterText="Total Amount" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="#cc0000">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Amount" FilterControlAltText="Filter colAmount column" HeaderText="Amount" UniqueName="colAmount" Aggregate="Sum" FooterText=" " FooterStyle-ForeColor="#cc0000" FooterStyle-Font-Bold="true">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="btnEdit" FilterControlAltText="Filter colEdit column" HeaderText="Edit" ImageUrl="Images/Edit.png" UniqueName="colEdit">
                                                    </telerik:GridButtonColumn>
                                                    <telerik:GridButtonColumn ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="btnDelete" ImageUrl="Images/Delete.png" ConfirmText="Are You Sure To Delete?" FilterControlAltText="Filter colDelete column" HeaderText="Delete" UniqueName="colDelete" ConfirmDialogType="RadWindow"></telerik:GridButtonColumn>
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                        
                                        
                                      
                                    </div>
                                
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                                  <div class="form-group" id="divButtonForMasterSave" runat="server">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="Button1" runat="server" ClientIDMode="Static" CssClass="btn btn-success" Text="Save Voucher Master Information" OnClick="btnSaveVoucherMasterInfomation_Click" />

                            <input class="btn btn-warning" runat="server" onserverclick="btnVoucherMasterInfomationClear_Click" type="button" value="Clear Master Information" />
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>
    <%-- <script>
        $(document).ready(function () {
           
            });
        });
    </script>--%>
</asp:Content>
