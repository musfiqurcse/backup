﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Balancika.Bll;
using BALANCIKA.BLL;
using System.Data;
using Microsoft.Reporting.WebForms;
using Balancika.BLL;
using Balancika.DAL;
using BALANCIKA.DAL;
using System.Data.SqlClient;
using System.Configuration;

namespace Balancika.WebApplication.Reports
{
    public partial class reportTesting : System.Web.UI.Page
    {
        private bool isNewEntry;
        private Users user;
        private Company _company = new Company();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["customVoucherMessage"] != null)
            {
                string msg = (string)Session["customVoucherMessage"];
                Alert.Show(msg);
                Session["customVoucherMessage"] = null;
            }
            _company = (Company)Session["Company"];
            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx");
                }
                else
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx?" + str);
                }

            }

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/Report1.rdlc");

            DataSet ds = new DataSet();
            string conString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select * From Ledger";
            cmd.CommandType = CommandType.Text;
            
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            adpt.Fill(ds);

            ReportDataSource rds = new ReportDataSource();
            rds.Name = "DataSet1";
            rds.Value = ds.Tables[0];

            
           
            ReportParameter rpt = new ReportParameter("CompanyId", _company.CompanyId.ToString());
            this.ReportViewer1.LocalReport.SetParameters(rpt);
            this.ReportViewer1.LocalReport.DataSources.Add(rds);
            if (!IsPostBack)
            {
                ReportViewer1.LocalReport.Refresh();
            }
           

            //con.Close();
            //con.Dispose();
            
        }
       
        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;

            }
            user = (Users)Session["user"];
            return user.UserId != 0;

        }

        
    }
}