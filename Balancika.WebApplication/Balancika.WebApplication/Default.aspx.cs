﻿using Balancika.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Balancika.WebApplication
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private bool isNewEntry;
        private Users _user;
        private Company _company = new Company();
        protected void Page_Load(object sender, EventArgs e)
        {
            _company = (Company)Session["Company"];
            _user = (Users)Session["user"];
            if (Session["savedListMessage"] != null)
            {
                string msg = Session["savedListMessage"].ToString();
                Alert.Show(msg);
                Session["savedListMessage"] = null;
            }
            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx");
                else
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx?" + str);
            }
            //if (!IsPostBack)
            //{
            //    if (Request.QueryString["id"] != null)
            //    {
            //        string listId = Request.QueryString["id"].ToString();

            //        ListTable objTable = new ListTable().GetListTableById(int.Parse(listId), _company.CompanyId);
            //        if (objTable != null || objTable.Id != 0)
            //        {
            //            lblId.Text = objTable.Id.ToString();
            //            txtListType.Value = objTable.ListType;
            //            txtListValue.Value = objTable.ListValue;
            //        }
            //    }
            //}

        }
        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;
            }

            _user = (Users)Session["user"];

            return _user.UserId != 0;
        }
    }
}