﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="LedgerReportingShow.aspx.cs" Inherits="Balancika.WebApplication.LedgerReportingShow" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI.Skins" Assembly="Telerik.Web.UI.Skins, Version=2015.1.225.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Register TagPrefix="telerik1" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2015.1.225.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Register TagPrefix="rsweb" Namespace="Microsoft.Reporting.WebForms" Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
       
        <div class="box">

            <div class="box-header with-border">
                <h3 class="box-title">Ledger Reporting</h3>
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group ">
                        <label for="selectChartOfAccount" class="col-sm-4 control-label">Ledger for Account</label>
                        <div class="col-xs-8">
                            <%--<input type="email" class="form-control" id="txtUpdate" name="email" placeholder="Enter a valid email address" runat="server" />
                            --%>
                            <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server">
                                <telerik1:RadDropDownList ID="selectChartOfAccount"
                                        name="selectChartOfAccount"
                                        
                                        runat="server" padding-left="20px"
                                        Width="100%"
                                        AutoPostBack="true"
                                        DefaultMessage="Select Ledger For Account"
                                        Skin="Bootstrap">
                                    </telerik1:RadDropDownList>

                            </telerik:RadAjaxPanel>
                        </div>

                    </div>
                </div>
                 <div class="col-md-6">
                        <div class="form-group ">
                            <label for="fromDate" class="col-sm-4 control-label">From Date</label>
                            <div class="col-xs-8">
                                <%--<input type="email" class="form-control" id="txtUpdate" name="email" placeholder="Enter a valid email address" runat="server" />
                                --%>
                                <telerik1:RadDatePicker  ID="fromDate" runat="server" Width="100%" Skin="Bootstrap" SelectedDate='<%# System.DateTime.Today %>'></telerik1:RadDatePicker>
                            </div>

                        </div>
                    </div>
                   <div class="col-md-6">
                        <div class="form-group ">
                            <label for="toDate" class="col-sm-4 control-label">To Date</label>
                            <div class="col-xs-8">
                                <telerik1:RadDatePicker  ID="toDate" runat="server" Width="100%" Skin="Bootstrap" SelectedDate='<%# System.DateTime.Today %>'></telerik1:RadDatePicker>
                            </div>

                        </div>
                    </div>
                <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="saveAccountsButton" runat="server" ClientIDMode="Static" CssClass="btn btn-success" Text="Show Ledger" OnClick="btnShow_Click" />
                           
                        </div>
                    </div>
                <div class="row">
                     <rsweb:ReportViewer ID="ReportViewer1" runat="server" AsyncRendering="false" SizeToReportContent="true"></rsweb:ReportViewer>
                </div>
                
                

            </div>
        </div>
    </form>
</asp:Content>
