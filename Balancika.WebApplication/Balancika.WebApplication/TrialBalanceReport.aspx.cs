﻿using Balancika.BLL;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Balancika.WebApplication
{
    public partial class TrialBalanceReport : System.Web.UI.Page
    {
        private bool isNewEntry;
        private Users user;
        private Company _company = new Company();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["customVoucherMessage"] != null)
            {
                string msg = (string)Session["customVoucherMessage"];
                Alert.Show(msg);
                Session["customVoucherMessage"] = null;
            }
            _company = (Company)Session["Company"];
            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx");
                }
                else
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx?" + str);
                }

            }

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/trialBalance.rdlc");

            DataSet ds = new DataSet();
            string conString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select * From Ledger";
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            adpt.Fill(ds);

            ReportDataSource rds = new ReportDataSource();
            rds.Name = "trialBalanceDataSet";
            rds.Value = ds.Tables[0];



            ReportParameter rpt = new ReportParameter("CompanyId", _company.CompanyId.ToString());
            
            this.ReportViewer1.LocalReport.SetParameters(rpt);
            this.ReportViewer1.LocalReport.DataSources.Add(rds);
            if (!IsPostBack)
            {
                ReportViewer1.LocalReport.Refresh();
            }


            //con.Close();
            //con.Dispose();

        }

        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;

            }
            user = (Users)Session["user"];
            return user.UserId != 0;

        }
    }
}