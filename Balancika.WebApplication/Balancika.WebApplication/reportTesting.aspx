﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="reportTesting.aspx.cs" Inherits="Balancika.WebApplication.Reports.reportTesting" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <h2>Report</h2>
       <%-- <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
            <LocalReport ReportEmbeddedResource="Balancika.WebApplication.Reports.LedgerDetails.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource Name="LedgerData" DataSourceId="ObjectDataSource1"></rsweb:ReportDataSource>
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource runat="server" SelectMethod="GetAllLedger" TypeName="Balancika.Bll.Ledger, Balancika.BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" ID="ObjectDataSource1">
            
        </asp:ObjectDataSource>--%>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" AsyncRendering="false" SizeToReportContent="true"></rsweb:ReportViewer>
    </div>
    </form>
</body>
</html>
