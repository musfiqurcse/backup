﻿using Balancika.Bll;
using Balancika.BLL;
using BALANCIKA.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Balancika.WebApplication
{
    public partial class ChequeInfo : System.Web.UI.Page
    {
        private Users _user;
        private Company _company;


        protected void Page_Load(object sender, EventArgs e)
        {
            _company = (Company)Session["Company"];
            _user = (Users)Session["User"];

            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx");
                else
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx?" + str);
            }
            if (!IsPostBack)
            {
                this.LoadFirstTime();
                if (Request.QueryString["id"] != null)
                {
                    string chequeID = Request.QueryString["id"].ToString();               

                    VoucherChequeInfo objVoucherCheque = new VoucherChequeInfo().GetVoucherChequeInfoByChequeId(int.Parse(chequeID));

                    lblChequeDetailsIdHidden.Text = objVoucherCheque.VoucherId.ToString();

                     VoucherMaster objVoucherMaster = new VoucherMaster().GetVoucherMasterByVoucherId(int.Parse(lblChequeDetailsIdHidden.Text), _company.CompanyId);

                    if (objVoucherMaster != null || objVoucherMaster.VoucherId != 0 || objVoucherCheque != null || objVoucherCheque.VoucherId != 0)
                    {
                        voucherDatePicker.SelectedDate = Convert.ToDateTime(objVoucherMaster.VoucherDate);
                        ddlPaymentMode.SelectedValue = objVoucherMaster.PaymentMode;
                        txtVoucherDescription.Value = objVoucherMaster.VoucherDescription;
                        costCenterDropDownlist.SelectedIndex = objVoucherMaster.CostCenterId;
                        txtVoucherTotal.Value = objVoucherMaster.VoucherTotal;
                        txtPaymentRef.Value = objVoucherMaster.PaymentRef;
                        //ddlSourceOfAccount.SelectedItem.Value = objVoucherMaster.SourceOfAccount;
                       // ddlBankAccountList.SelectedItem.Value = objVoucherCheque.BankName;
                        txtBranchName.Value = objVoucherCheque.Branch;
                        txtBankAccountNumber.Value = objVoucherCheque.AccountNo;
                        txtChequeNumber.Value = objVoucherCheque.ChequeNo;
                        txtChequeAmount.Value = objVoucherCheque.ChequeAmount.ToString();
                        
                        SetIndex(ddlSourceOfAccount,objVoucherMaster.SourceCOAId.ToString());
                        SetIndex(ddlBankAccountList,objVoucherCheque.BankId.ToString());
                       
                        ddlChequeType.SelectedValue = objVoucherCheque.ChequeType;
                        chequeSubmitDate.SelectedDate = Convert.ToDateTime(objVoucherCheque.SubmiteDate);
                    }
                }
            }

        }
        public void SetIndex(Telerik.Web.UI.RadDropDownList aDowList, string val)
        {

            for (int i = 0; i < aDowList.Items.Count; i++)
            {
                var li = aDowList.Items[i];
                if (li.Value == val)
                {
                    aDowList.SelectedIndex = i;
                    break;
                }
            }
        }
        private void LoadFirstTime()
        {
            LoadPaymentModeDropDownList();
            this.LoadChartOfAccountList();
            LoadCostCenterDropDownList();
            loadBankDropdownList();
            loadChequeDropdown();
        }

        private void loadChequeDropdown()
        {


            ddlChequeType.Items.Add("Account Payee");
            ddlChequeType.Items.Add("Bearer");
        }
        private void loadBankDropdownList()
        {

            List<Bank> objBankAccounts = new Bank().GetAllBank(_company.CompanyId);
            ddlBankAccountList.DataSource = objBankAccounts;
            ddlBankAccountList.DataTextField = "BankName";
            ddlBankAccountList.DataValueField = "BankId";
            ddlBankAccountList.DataBind();
        }
        private void LoadCostCenterDropDownList()
        {
            List<CostCenter> costCenterList = new CostCenter().GetAllCostCenter(_company.CompanyId);
            costCenterDropDownlist.DataSource = costCenterList;
            costCenterDropDownlist.DataTextField = "CostCenterName";
            costCenterDropDownlist.DataValueField = "CostCenterId";
            costCenterDropDownlist.DataBind();
        }

        private void LoadPaymentModeDropDownList()
        {
            ddlPaymentMode.Items.Clear();
            ddlPaymentMode.Items.Add("Cash");
            ddlPaymentMode.Items.Add("Bank");
        }

        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;
            }

            _user = (Users)Session["user"];

            return _user.UserId != 0;
        }

        protected void ddlPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
           // this.LoadChartOfAccountList();
        }

        private void LoadChartOfAccountList()
        {
            List<ChartOfAccount> coaList = new ChartOfAccount().GetAllChartOfAccount(_company.CompanyId);
            ddlSourceOfAccount.DataSource = coaList;
            ddlSourceOfAccount.DataTextField = "CoaTitle";
            ddlSourceOfAccount.DataValueField = "CoaId";
            ddlSourceOfAccount.DataBind();
        }

        protected void btnUpdateCheque_Click(object sender, EventArgs e)
        {
            //try
            //{
                VoucherMaster objVoucherMaster = new VoucherMaster();

                objVoucherMaster.VoucherId = int.Parse(lblChequeDetailsIdHidden.Text);
                objVoucherMaster.VoucherDate = voucherDatePicker.SelectedDate.ToString();
                objVoucherMaster.VoucherType = "Recieve Voucher";
                objVoucherMaster.CompanyId = _company.CompanyId;

                objVoucherMaster.CostCenterId = int.Parse(costCenterDropDownlist.SelectedIndex == -1 ? "0" : costCenterDropDownlist.SelectedItem.Value);
                objVoucherMaster.PaymentMode = ddlPaymentMode.SelectedItem.Value.ToString(); //paymentModeDropDownList.SelectedText.ToString();
                objVoucherMaster.VoucherDescription = txtVoucherDescription.Value;

                objVoucherMaster.UpdateBy = _user.UserId;
                objVoucherMaster.UpdateDate = DateTime.Now;
                objVoucherMaster.ApprovedBy = _user.UserId;
                objVoucherMaster.ApprovedDate = DateTime.Now;
                objVoucherMaster.PostDate = DateTime.Now;
                objVoucherMaster.VoucherTotal = txtVoucherTotal.Value;
                objVoucherMaster.PaymentRef = txtPaymentRef.Value;
                objVoucherMaster.IsPosted = true;
                objVoucherMaster.Status = "Checked";
                objVoucherMaster.SourceCOAId = int.Parse(ddlSourceOfAccount.SelectedItem.Value);
                objVoucherMaster.SourceOfAccount = "Cheque";
                objVoucherMaster.VoucherNo = "12548";
                //objVoucherMaster.SourceOfAccount = ddlSourceOfAccount.SelectedValue;
                
                objVoucherMaster.SourceCOAId = 1;
                objVoucherMaster.UpdateVoucherMaster();


                VoucherChequeInfo objVoucherCheque = new VoucherChequeInfo();


                objVoucherCheque.ChequeId = int.Parse(Request.QueryString["id"]);
                objVoucherCheque.VoucherType = "Recieved Voucher";
                objVoucherCheque.VoucherId = int.Parse(lblChequeDetailsIdHidden.Text);
                objVoucherCheque.BankId = int.Parse(ddlBankAccountList.SelectedItem.Value);
                objVoucherCheque.BankName = ddlBankAccountList.SelectedItem.Text;
                objVoucherCheque.Branch = txtBranchName.Value;
                objVoucherCheque.AccountNo = txtBankAccountNumber.Value;
                objVoucherCheque.ChequeNo = txtChequeNumber.Value;
                objVoucherCheque.ChequeType = ddlChequeType.SelectedItem.Text;
                objVoucherCheque.ChequeAmount = Decimal.Parse(txtChequeAmount.Value);
                objVoucherCheque.IsPostToLedger = false;
                objVoucherCheque.SubmiteDate = chequeSubmitDate.SelectedDate.Value.ToShortDateString();
                objVoucherCheque.SubmiteToBank = txtSubmitBank.Checked==true?1:0;
                objVoucherCheque.UpdateVoucherChequeInfo();
                //loadChequeDetailsGrid();
            //}
            //catch (Exception ex)
            //{
            //    Alert.Show(ex.Message);
            //}


        }

      
    }
}