﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Balancika.Bll;
using Balancika.BLL;
using BALANCIKA.BLL;
using Telerik.Web.UI;

namespace Balancika.WebApplication
{
    public partial class ChequeList : System.Web.UI.Page
    {
        private bool isNewEntry;
        private Users user;
        private Company _company = new Company();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["customVoucherMessage"] != null)
            {
                string msg = (string)Session["customVoucherMessage"];
                Alert.Show(msg);
                Session["customVoucherMessage"] = null;
            }
            _company = (Company)Session["Company"];
            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx");
                }
                else
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx?" + str);
                }

            }
            if (!IsPostBack)
            {
                this.LoadFirstTime();
            }


        }

        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;

            }
            user = (Users)Session["user"];
            return user.UserId != 0;

        }
        private void LoadFirstTime()
        {
            this.loadChequeDetailsGrid();
        }

        private void loadChequeDetailsGrid()
        {
            VoucherChequeInfo objChequeInfo = new VoucherChequeInfo();

            chequeDetailsGrid.DataSource = objChequeInfo.GetAllVoucherChequeInfo();
            chequeDetailsGrid.DataBind();
        }

        protected void chequeDetailsGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string id = string.Empty;
            if (e.CommandName == "btnEdit" || e.CommandName == "btnDelete")
            {
                GridDataItem item = (GridDataItem)e.Item;

                id = item["colId"].Text;

            }

            switch (e.CommandName)
            {
                case "btnEdit":
                    //VoucherChequeInfo objVoucherCheque = new VoucherChequeInfo();
                    //var x = objVoucherCheque.GetVoucherChequeInfoByChequeId(int.Parse(id));
                    //ddlBankAccountList.SelectedValue = x.BankName;
                    //txtBranchName.Value = x.Branch;
                    //txtBankAccountNumber.Value = x.AccountNo;
                    //txtChequeNumber.Value = x.ChequeNo;
                    //txtChequeAmount.Value = x.ChequeAmount.ToString();
                    //txtIsPostToLedger.Checked = x.IsPostToLedger;
                    //chequeSubmitDate.SelectedDate = Convert.ToDateTime(x.SubmiteDate);
                    //lblChequeDetailsIdHidden.Text = x.ChequeId.ToString();
                    //lblId1.Text = x.VoucherId.ToString();
                    //btnCheckDetailsSave.Visible = false;
                    //btnUpdateChequeDiv.Visible = true;
                    //divChequeMasterSaveButton.Visible = false;
                    Response.Redirect("ChequeInfo.aspx?id=" + id, true);
                    break;
                case "btnDelete":
                    VoucherChequeInfo objChequeInfo = new VoucherChequeInfo().GetVoucherChequeInfoByChequeId(int.Parse(id));
                    int delete1 = new VoucherMaster().DeleteVoucherMasterByVoucherId(objChequeInfo.VoucherId);
                    int delete = new VoucherChequeInfo().DeleteVoucherChequeInfoByChequeId(int.Parse(id));
                    if (delete == 0 && delete1 == 0)
                    {
                        Alert.Show("Data is not deleted...!");
                    }
                    else
                    {
                        chequeDetailsGrid.DataSource = new VoucherChequeInfo().GetAllVoucherChequeInfo();
                        chequeDetailsGrid.DataBind();
                    }
                    break;
                default:
                    break;
            }
        }
    }
}