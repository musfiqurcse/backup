﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Balancika.BLL;
using Telerik.Web.UI;

namespace Balancika.WebApplication
{
    public partial class BankDetailsAccountInformation : System.Web.UI.Page
    {
        private static bool isNewEntry = true;
        private static int userId;
        private Users _user;
        List<Bank> objBankList = new List<Bank>();
        private Company _company = new Company();
        protected void Page_Load(object sender, EventArgs e)
        {
            _company = (Company)Session["Company"];

            AccountInfoPageDiv.Disabled = true;
            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                    Response.Redirect("LogIn.aspx?refPage=default.aspx");
                else
                    Response.Redirect("LogIn.aspx?refPage=default.aspx?" + str);
            }
           
            if (!IsPostBack)
            {
                this.LoadAccountTypeDropDown();
                this.LoadBankTable();
                this.LoadBankNameDropDownList();
                this.LoadBankNames();
                this.LoadAccountsTable();        
            }
                
        }
        public void SetIndex(Telerik.Web.UI.RadDropDownList aDowList, string val)
        {

            for (int i = 0; i < aDowList.Items.Count; i++)
            {
                var li = aDowList.Items[i];
                if (li.Value == val)
                {
                    aDowList.SelectedIndex = i;
                    break;
                }
            }
        }
        private void LoadAccountsTable()
        {
            try
            {

                BankAccounts accounts = new BankAccounts();
                //List<BankAccounts> allBankAccountInformation = accounts.GetAllBankAccounts(_company.CompanyId);
                bankAccountGrid.DataSource = accounts.GetAllBankAccounts(_company.CompanyId);
                bankAccountGrid.Rebind();

            }
            catch (Exception exc)
            {
                Alert.Show(exc.Message);
            }
        }
        void LoadBankNames()
        {

            List<Bank> allBanks = new Bank().GetAllBank(1);
            List<string> nameList = new List<string>();
            foreach (Bank bank in allBanks)
            {
                nameList.Add(bank.BankName);
            }
            bankIdRadDropDownList1.DataSource = nameList;
            bankIdRadDropDownList1.DataBind();
        }
        void LoadAccountTypeDropDown()
        {
            List<string> myList = new List<string>();

            myList.Add("Current");
            myList.Add("Savings");
            myList.Add("Marchent");

            accountTypeRadDropDownList1.DataSource = myList;
            accountTypeRadDropDownList1.DataBind();
        }
        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;
            }

            _user = (Users)Session["user"];

            return _user.UserId != 0;
        }

        protected void btnSaveAccount_Click(object sender, EventArgs e)
        {
            BankAccounts objAccounts = new BankAccounts();

            List<BankAccounts> myList = objAccounts.GetAllBankAccounts(_user.CompanyId);


            objAccounts.BankId = int.Parse(bankIdRadDropDownList1.SelectedItem.Value);
            objAccounts.BranchName = txtBranchName.Value;
            objAccounts.AccountNo = txtAccountNo.Value;
            objAccounts.AccountTitle = txtTitle.Value;
            objAccounts.AccountType = accountTypeRadDropDownList1.SelectedItem.Text;
            objAccounts.OpeningDate = (RadDatePicker2.SelectedDate.ToString());
            objAccounts.CompanyId = _company.CompanyId;
            objAccounts.IsActive = chkIsActive.Checked;





            objAccounts.BankAccountId = objAccounts.GetMaxAccountId() + 1;
            int success = objAccounts.InsertBankAccounts();

            if (success > 0)
            {
                this.LoadAccountsTable();
                Session["saveBankAccountInfo"] = ("Accounts info Saved successfully");
                Response.Redirect(Request.RawUrl);


                //
                this.Clear();
            }
            else
            {
                Alert.Show("Error occured");
            }
        }


        protected void btnClear_Click(object sender, EventArgs e)
        {

        }

        protected void btnBankInfoClear_Click(object sender, EventArgs e)
        {
        }

        protected void btnSaveBankInfo_Click(object sender, EventArgs e)
        {
            Bank objBank = new Bank();

            List<Bank> myList = objBank.GetAllBank(_company.CompanyId);
            int bankId = myList.Count;

            objBank.BankId = bankId;
            objBank.BankCode = txtBankCode.Value;
            objBank.BankName = txtBankName.Value;
            objBank.ContactPerson = txtContactPerson.Value;
            objBank.ContactDesignation = txtDesignation.Value;
            objBank.ContactNo = txtContactNo.Value;
            objBank.ContactEmail = txtEmail.Value;
            objBank.CompanyId = _company.CompanyId;
            objBank.UpdatedBy = _user.UserId;
            objBank.UpdatedDate = DateTime.Today;
            objBank.IsActive = chkIsActive.Checked;



            int sucess = 0;
            sucess = objBank.InsertBank();

            if (sucess > 0)
            {
                Alert.Show("Bank info Saved successfully");
                this.Clear();
                this.LoadBankTable();
                this.LoadBankNames();
                AccountInfoPageDiv.Disabled = false;
            }
        }
        private void LoadBankTable()
        {
            Bank objBank = new Bank();
            objBankList = objBank.GetAllBank(_company.CompanyId);
            bankInfoGrid.DataSource = objBankList;
            bankInfoGrid.DataBind();

        }
        private void Clear()
        {
            labelTxt.Text = "";
            txtBankCode.Value = "";
            txtBankName.Value = "";
            txtContactPerson.Value = "";
            txtDesignation.Value = "";
            txtContactNo.Value = "";
            txtEmail.Value = "";
            chkIsActive.Checked = false;

        }

        protected void bankInfoGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {

        }

        public void LoadBankNameDropDownList()
        {
            List<Bank> objBankList = new Bank().GetAllBank(_company.CompanyId);
            bankIdRadDropDownList1.DataSource = objBankList;
            bankIdRadDropDownList1.DataTextField = "BankName";
            bankIdRadDropDownList1.DataValueField = "BankId";
            bankIdRadDropDownList1.DataBind();
        }

      

        protected void bankAccountGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string id1 = string.Empty;
            if (e.CommandName == "btnEdit" || e.CommandName == "btnDelete")
            {
                GridDataItem item = (GridDataItem)e.Item;
                id1 = item["colBankID"].Text;
            }
            switch (e.CommandName)
            {
                case "btnEdit":
                    BankAccounts editBankAccount = new BankAccounts();
                    var x = editBankAccount.GetBankAccountsByBankAccountId(int.Parse(id1), _company.CompanyId);
                    SetIndex(bankIdRadDropDownList1, x.BankId.ToString());
                    Session["editBankAccountId"] = x.BankAccountId;
                    txtBranchName.Value = x.BranchName;
                    txtAccountNo.Value = x.AccountNo;
                    txtTitle.Value = x.AccountTitle;
                    RadDatePicker2.SelectedDate = Convert.ToDateTime(x.OpeningDate);
                    break;
                case "btnDelete":
                    break;

                default:
                    break;




            }


        }

  

        
    }
}