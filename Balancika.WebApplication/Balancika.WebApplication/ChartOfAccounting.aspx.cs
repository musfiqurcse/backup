﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Balancika.BLL;
using BALANCIKA.BLL;
using Telerik.Web.UI;


namespace Balancika.WebApplication
{
    public partial class ChartOfAccounting : System.Web.UI.Page
    {
        private bool isNewEntry;
        private Users _user;
        private List<Company> allCompanyList = new List<Company>();
        private Company _company = new Company();
        protected void Page_Load(object sender, EventArgs e)
        {
            _company = (Company)Session["Company"];
            _user = (Users)Session["user"];
            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                    Response.Redirect("LogIn.aspx?refPage=default.aspx");
                else
                    Response.Redirect("LogIn.aspx?refPage=default.aspx?" + str);
            }
        
            this.LoadAccountGroup();
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    string CoaGroupId = Request.QueryString["id"].ToString();

                    ChartOfAccount coa = new ChartOfAccount().GetChartOfAccountByCoaId(int.Parse(CoaGroupId), _company.CompanyId);
                    if (coa != null || coa.CoaGroupId != 0)
                    {

                        lblId.Text = coa.CoaId.ToString();

                       
                        SetIndex(chartOfAccountGroupIdDropDownList,coa.CoaGroupId.ToString());
                        LoadType(coa.CoaGroupId);
                        if(coa.CoaId!=coa.ParentId)
                        SetIndex(chartOfAccountTypeDropDownList,coa.ParentId.ToString());
                        txtChartOfAccountCode.Value = coa.CoaCode;
                        txtChartOfAccountTitle.Value = coa.CoaTitle;

                    }
                }
            }
            
           
           
        }
        public void SetIndex(Telerik.Web.UI.RadDropDownList aDowList, string val)
        {

            for (int i = 0; i < aDowList.Items.Count; i++)
            {
                var li = aDowList.Items[i];
                if (li.Value == val)
                {
                    aDowList.SelectedIndex = i;
                    break;
                }
            }
        }
        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;
            }

            _user = (Users)Session["user"];

            return _user.UserId != 0;
        }

        private void LoadAllCompany()
        {
            Company company = new Company();
            allCompanyList = company.GetAllCompany();
            List<string> nameList = new List<string>();
            foreach (Company com in allCompanyList)
            {
                nameList.Add(com.CompanyName);
            }


        }
      
        private void LoadAccountGroup()
        {
               List<ChartOfAccountGroup> accountGroups = new ChartOfAccountGroup().GetAllChartOfAccountGroup(_company.CompanyId);
            
            chartOfAccountGroupIdDropDownList.DataSource = accountGroups;
            chartOfAccountGroupIdDropDownList.DataTextField = "CoaGroupName";
            chartOfAccountGroupIdDropDownList.DataValueField = "CoaGroupId";
            chartOfAccountGroupIdDropDownList.DataBind();
        }



        protected void chartOfAccountTypeDropDownList_ItemSelected(object sender, DropDownListEventArgs e)
        {

        }

        protected void chartOfAccountTypeDropDownList_SelectedIndexChanged(object sender, DropDownListEventArgs e)
        {
            

        }

        protected void chartOfAccountGroupIdDropDownList_ItemSelected(object sender, DropDownListEventArgs e)
        {
            

        }

        protected void chartOfAccountGroupIdDropDownList_SelectedIndexChanged(object sender, DropDownListEventArgs e)
        {
            LoadType(int.Parse(chartOfAccountGroupIdDropDownList.SelectedItem.Value));
        }

        protected void parentIdDropDownList_ItemSelected(object sender, DropDownListEventArgs e)
        {

        }

        protected void parentIdDropDownList_SelectedIndexChanged(object sender, DropDownListEventArgs e)
        {

        }

        protected void companyIdRadDropDownList1_ItemSelected(object sender, DropDownListEventArgs e)
        {

        }

        protected void companyIdRadDropDownList1_SelectedIndexChanged(object sender, DropDownListEventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (chartOfAccountGroupIdDropDownList.SelectedIndex == -1)
            {
                Alert.Show("Select a Chart of Account Group");

            }
            else
            {

                ChartOfAccount objChartOfAccount = new ChartOfAccount();
                List<ChartOfAccount> myAccounts = objChartOfAccount.GetAllChartOfAccount(_company.CompanyId);

                objChartOfAccount.CoaId = myAccounts.Count;



                objChartOfAccount.CoaGroupId = int.Parse(chartOfAccountGroupIdDropDownList.SelectedItem.Value);
                objChartOfAccount.CoaCode = txtChartOfAccountCode.Value;
                objChartOfAccount.CoaTitle = txtChartOfAccountTitle.Value;
                objChartOfAccount.CoaType = (chartOfAccountTypeDropDownList.SelectedIndex == -1
                    ? objChartOfAccount.CoaTitle
                    : chartOfAccountTypeDropDownList.SelectedItem.Text);

                objChartOfAccount.IsActive = chkIsActive.Checked;
                objChartOfAccount.UpdateBy = _user.UserId;
                objChartOfAccount.UpdateDate = DateTime.Now;
                objChartOfAccount.CompanyId = _company.CompanyId;

                int sucess = 0;
                if (lblId.Text == "" || lblId.Text == "0")
                {
                    objChartOfAccount.CoaId = new ChartOfAccount().GetMaxCoaId() + 1;
                    objChartOfAccount.ParentId = (chartOfAccountTypeDropDownList.SelectedIndex == -1
                        ? objChartOfAccount.CoaId
                        : int.Parse(chartOfAccountTypeDropDownList.SelectedItem.Value));

                    sucess = objChartOfAccount.InsertChartOfAccount();

                    if (sucess > 0)
                    {
                        Alert.Show("Chart Of Account saved successfully");
                        this.Clear();
                    }
                }
                else
                {
                    objChartOfAccount.CoaId = int.Parse(lblId.Text);
                    objChartOfAccount.ParentId = (chartOfAccountTypeDropDownList.SelectedIndex == -1
                        ? objChartOfAccount.CoaId
                        : int.Parse(chartOfAccountTypeDropDownList.SelectedItem.Value));
                    sucess = objChartOfAccount.UpdateChartOfAccount();


                    if (sucess > 0)
                    {
                        Response.Redirect("ChartOfAccountingInfoList.aspx", true);
                    }
                }
                chartOfAccountGroupIdDropDownList.SelectedIndex = -1;
                chartOfAccountTypeDropDownList.SelectedIndex = -1;

            }

        }

        void LoadType(int groupId)
        {
            List<ChartOfAccount> accList = new ChartOfAccount().GetAllChartOfAccountGroupId(groupId);
            chartOfAccountTypeDropDownList.DataSource = accList;
            chartOfAccountTypeDropDownList.DataTextField = "CoaTitle";
            chartOfAccountTypeDropDownList.DataValueField = "CoaId";
            chartOfAccountTypeDropDownList.DataBind();
        }
        private void Clear()
        {
            lblId.Text = "";

            txtChartOfAccountCode.Value = "";
            txtChartOfAccountTitle.Value = "";
        }


        protected void btnClear_Click(object sender, EventArgs e)
        {

        }
    }
}