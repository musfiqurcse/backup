﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Balancika.BLL;
using Microsoft.Reporting.WebForms;

namespace Balancika.WebApplication
{
    public partial class LedgerReportingShow : System.Web.UI.Page
    {
         private bool isNewEntry;
        private Users user;
        private Company _company = new Company();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["customVoucherMessage"] != null)
            {
                string msg = (string) Session["customVoucherMessage"];
                Alert.Show(msg);
                Session["customVoucherMessage"] = null;
            }
            _company = (Company) Session["Company"];
            if (!isValidSession())
            {
                string str = Request.QueryString.ToString();
                if (str == string.Empty)
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx");
                }
                else
                {
                    Response.Redirect("LogIn.aspx?refPage=Default.aspx?" + str);
                }

            }

           this.LoadLedgerAccount();


            //con.Close();
            //con.Dispose();
        }
        private bool isValidSession()
        {
            if (Session["user"] == null)
            {
                return false;

            }
            user = (Users)Session["user"];
            return user.UserId != 0;

        }

        public void LoadReportDetails()
        {
             ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/LedgerReporting.rdlc");

            DataSet ds = new DataSet();
            string conString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select * From TempLedgerReportTable";
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            adpt.Fill(ds);

            ReportDataSource rds = new ReportDataSource();
            rds.Name = "DataSet1";
            rds.Value = ds.Tables[0];



            ReportParameter rpt = new ReportParameter("Report_Parameter_MasterAccountId", selectChartOfAccount.SelectedItem.Value);
            ReportParameter rpt1 = new ReportParameter("Report_From_Date", fromDate.SelectedDate.ToString());
            ReportParameter rpt2 = new ReportParameter("Report_To_Date", toDate.SelectedDate.ToString());

            this.ReportViewer1.LocalReport.SetParameters(new ReportParameter[]{rpt,rpt1,rpt2});
            this.ReportViewer1.LocalReport.DataSources.Add(rds);
            if (!IsPostBack)
            {
                ReportViewer1.LocalReport.Refresh();
            }
        }

        public void LoadLedgerAccount()
        {
            List<ChartOfAccount> coAccounts = new ChartOfAccount().GetAllChartOfAccount(_company.CompanyId);
            selectChartOfAccount.DataSource = coAccounts;
            selectChartOfAccount.DataTextField = "CoaTitle";
            selectChartOfAccount.DataValueField = "CoaId";
            selectChartOfAccount.DataBind();
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            this.LoadReportDetails();
        }
    }
}