﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ChequeInfo.aspx.cs" Inherits="Balancika.WebApplication.ChequeInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="box">
            <div id="divVoucherMasterInformation" runat="server" class="box box-primary" style="width: auto">

                <div class="box-header with-border" style="width: 100%">
                    <h1 style="font-size: 20px; font-weight: bold; color: green">Update <b>Voucher</b> Information </h1>
                </div>
                <div id="divVoucherMasterInformationBody" class="box-body" runat="server">
                    <div class="col-sm-6">
                        <div class="form-group ">
                            <label for="voucherDatePicker" class="col-sm-4 control-label">Voucher Date</label>
                            <div class="col-xs-8">
                                <telerik:RadDatePicker Skin="Bootstrap" ID="voucherDatePicker" runat="server" Width="100%" SelectedDate='<%# System.DateTime.Today %>'></telerik:RadDatePicker>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group ">
                            <label for="paymentModeDropDownList" class="col-sm-4 control-label">Payment Mode</label>
                            <div class="col-xs-8">
                                <asp:DropDownList ID="ddlPaymentMode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPaymentMode_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="txtVoucherDescription" class="col-sm-4 control-label">Voucher Description</label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control" width="60px" name="txtVoucherDescription" id="txtVoucherDescription" placeholder="Description" runat="server"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group ">
                            <label for="costCenterDropDownlist" class="col-sm-4 control-label">Cost Center</label>
                            <div class="col-xs-8">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server">
                                    <telerik:RadDropDownList ID="costCenterDropDownlist"
                                        Skin="Bootstrap"
                                        runat="server" padding-left="20px"
                                        Width="100%"
                                        AutoPostBack="true"
                                        DefaultMessage="Select Cost Center">
                                    </telerik:RadDropDownList>
                                </telerik:RadAjaxPanel>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtVoucherTotal" class="col-sm-4 control-label">Voucher Total</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtVoucherTotal" id="txtVoucherTotal" placeholder="Voucher Total" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtPaymentRef" class="col-sm-4 control-label">Payment Reference</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtPaymentRef" id="txtPaymentRef" placeholder="Payment Reference" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtPaymentRef" class="col-sm-4 control-label">Source Of Account</label>
                            <div class="col-lg-8">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel4" runat="server">
                                    <telerik:RadDropDownList ID="ddlSourceOfAccount"
                                        Skin="Bootstrap"
                                        runat="server" padding-left="20px"
                                        Width="100%"
                                        AutoPostBack="true"
                                        DefaultMessage="Select Source Of Account">
                                    </telerik:RadDropDownList>
                                </telerik:RadAjaxPanel>

                            </div>

                        </div>
                    </div>
                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>

                    <%--<div class="col-md-6">
                            <div class="form-group ">
                                <label for="chkCheque" class="col-sm-4 control-label">Cheque Details</label>
                                <div class="col-lg-8">
                                    <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged" />
                                </div>

                            </div>--%>
                </div>
            </div>


            <%-- Introducing Cheque Details --%>

            <div id="divChequeDetails" runat="server" class="box box-primary" style="width: auto">
                <div class="box-header with-border" style="width: 100%">
                    <h1 style="font-size: 20px; font-weight: bold; color: green">Update  <b>Cheque</b> Details Information </h1>
                </div>
                <div id="divChequeDetailsBody" class="box-body" runat="server">
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtVoucherName" class="col-sm-4 control-label">Bank Name</label>
                            <div class="col-lg-8">
                                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                                    <telerik:RadDropDownList ID="ddlBankAccountList" runat="server"
                                        AutoPostBack="true"
                                        Skin="Bootstrap"
                                        Width="100%"
                                        padding-left="20px">
                                    </telerik:RadDropDownList>
                                </telerik:RadAjaxPanel>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtBranchName" class="col-sm-4 control-label">Branch Name</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtBranchName" id="txtBranchName" placeholder="Enter Branch Name" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtBankAccountNumber" class="col-sm-4 control-label">Account Number</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtBankAccountNumber" id="txtBankAccountNumber" placeholder="Enter Account Number" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtChequeNumber" class="col-sm-4 control-label">Cheque No</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtChequeNumber" id="txtChequeNumber" placeholder="Enter Cheque Number" runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtChequeType" class="col-sm-4 control-label">Cheque Type</label>
                            <div class="col-lg-8">
                                <asp:DropDownList ID="ddlChequeType" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtChequeAmount" class="col-sm-4 control-label">Cheque Amount</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" width="60px" name="txtChequeAmount" id="txtChequeAmount" placeholder="Enter Account Number" runat="server" clientidmode="Static" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtSubmitDate" class="col-sm-4 control-label">Submit Date</label>
                            <div class="col-lg-8">
                                <telerik:RadDatePicker Skin="Bootstrap" ID="chequeSubmitDate" runat="server" Width="100%" SelectedDate='<%# System.DateTime.Today %>'></telerik:RadDatePicker>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label for="txtIsPostToLedger" class="col-sm-4 control-label">Submit To Bank</label>
                            <div class="col-lg-8">
                                <input type="checkbox" width="60px" name="txtSubmitBank" id="txtSubmitBank"  runat="server" clientidmode="Static" />
                            </div>

                        </div>
                    </div>


                    <div class="form-group" id="btnUpdateChequeDiv" runat="server">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="btnUpdateCheque" runat="server" Text="Update Details" CssClass="btn btn-success" OnClick="btnUpdateCheque_Click" />
                        </div>
                    </div>
                    <asp:Label ID="lblChequeDetailsIdHidden" runat="server" Text="Label" Visible="false"></asp:Label>

                </div>
            </div>
        </div>

    </form>
</asp:Content>
