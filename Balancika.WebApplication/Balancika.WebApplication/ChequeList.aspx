﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ChequeList.aspx.cs" Inherits="Balancika.WebApplication.ChequeList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
       <div class="row">
                        <div class="col-sm-12">
                            <telerik:RadGrid ID="chequeDetailsGrid" runat="server"
                                Skin="Bootstrap"
                                CssClass="table table-bordered table-hover"
                                AllowPaging="True"
                                GroupPanelPosition="Top"
                                AutoGenerateColumns="False"
                                OnItemCommand="chequeDetailsGrid_ItemCommand">
                                <MasterTableView ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="VoucherType" HeaderText="Voucher Type" UniqueName="colVoucherType" FilterControlAltText="Filter colVoucherType column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="BankName" HeaderText="Bank Name" UniqueName="colBankName" FilterControlAltText="Filter colBankName column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Branch" HeaderText="Branch" UniqueName="colBranch" FilterControlAltText="Filter colBranch column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AccountNo" HeaderText="Account Number" UniqueName="colAccountNumber" FilterControlAltText="Filter colAccountNumber column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ChequeNo" HeaderText="Cheque Number" UniqueName="colChequeNo" FilterControlAltText="Filter colChequeNo column" FooterText="Total Amount: "></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ChequeAmount" HeaderText="Cheque Amount" UniqueName="colCheckAmount" FilterControlAltText="Filter colCheckAmount column" Aggregate="Sum" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="#cc0000" FooterText=" "></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ChequeType" HeaderText="Cheque Type" UniqueName="colChequeType" FilterControlAltText="Filter colChequeType column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="SubmiteDate" HeaderText="Submit Date" UniqueName="colSubmitDate" FilterControlAltText="Filter colSubmitDate column"></telerik:GridBoundColumn>
                                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="btnEdit" ImageUrl="Images/Edit.png" FilterControlAltText="Filter colEdit column" HeaderText="Edit" UniqueName="colEdit"></telerik:GridButtonColumn>
                                        <telerik:GridButtonColumn ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="btnDelete" ImageUrl="Images/Delete.png" ConfirmText="Are You Sure To Delete?" FilterControlAltText="Filter colDelete column" HeaderText="Delete" UniqueName="colDelete" ConfirmDialogType="RadWindow"></telerik:GridButtonColumn>
                                        <telerik:GridBoundColumn DataField="ChequeId" HeaderText="ChequeID" Display="False" UniqueName="colId" FilterControlAltText="Filter colId column"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </div>
                    </div>
        </form>
</asp:Content>
