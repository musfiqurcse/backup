


ALTER PROC sp_TrialBalance
(

	@fromDate Date,
	@ToDate Date
)
AS
Begin
	SELECT SUM(CONVERT(Decimal(18,2),vm.VoucherTotal)) AS DEBIT, coa.CoaTitle FROM VoucherMaster AS vm inner join ChartOfAccount as coa on vm.SourceCOAId = coa.CoaId
	GROUP BY coa.CoaTitle, vm.VoucherDate, vm.VoucherType
	HAVING vm.VoucherType = 'Receipt' AND vm.VoucherDate BETWEEN @fromDate AND @ToDate

	SELECT SUM(CONVERT(Decimal(18,2),vm.VoucherTotal)) AS CREDIT, coa.CoaTitle, vm.PostDate FROM VoucherMaster AS vm inner join ChartOfAccount as coa on vm.SourceCOAId = coa.CoaId
	GROUP BY coa.CoaTitle, vm.VoucherDate, vm.VoucherType, vm.PostDate
	HAVING vm.VoucherType = 'Expense' AND vm.VoucherDate BETWEEN @fromDate AND @ToDate
End

EXEC sp_TrialBalance '2015-12-01', '2015-12-08'