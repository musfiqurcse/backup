USE [master]
GO
/****** Object:  Database [DrugLandDB]    Script Date: 12/24/2015 7:21:21 PM ******/
CREATE DATABASE [DrugLandDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DrugLandDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\DrugLandDB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DrugLandDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\DrugLandDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DrugLandDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DrugLandDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DrugLandDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DrugLandDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DrugLandDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DrugLandDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DrugLandDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [DrugLandDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DrugLandDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [DrugLandDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DrugLandDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DrugLandDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DrugLandDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DrugLandDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DrugLandDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DrugLandDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DrugLandDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DrugLandDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DrugLandDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DrugLandDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DrugLandDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DrugLandDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DrugLandDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DrugLandDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DrugLandDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DrugLandDB] SET RECOVERY FULL 
GO
ALTER DATABASE [DrugLandDB] SET  MULTI_USER 
GO
ALTER DATABASE [DrugLandDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DrugLandDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DrugLandDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DrugLandDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DrugLandDB', N'ON'
GO
USE [DrugLandDB]
GO
/****** Object:  StoredProcedure [dbo].[sp_TrialBalance]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[sp_TrialBalance]
(

	@fromDate Date,
	@ToDate Date
)
AS
Begin
	SELECT SUM(CONVERT(Decimal(18,2),vm.VoucherTotal)) AS DEBIT, coa.CoaTitle FROM VoucherMaster AS vm inner join ChartOfAccount as coa on vm.SourceCOAId = coa.CoaId
	GROUP BY coa.CoaTitle, vm.VoucherDate, vm.VoucherType
	HAVING vm.VoucherType = 'Receipt' AND vm.VoucherDate BETWEEN @fromDate AND @ToDate

	SELECT SUM(CONVERT(Decimal(18,2),vm.VoucherTotal)) AS CREDIT, coa.CoaTitle, vm.PostDate FROM VoucherMaster AS vm inner join ChartOfAccount as coa on vm.SourceCOAId = coa.CoaId
	GROUP BY coa.CoaTitle, vm.VoucherDate, vm.VoucherType, vm.PostDate
	HAVING vm.VoucherType = 'Expense' AND vm.VoucherDate BETWEEN @fromDate AND @ToDate
End
GO
/****** Object:  Table [dbo].[Addresses]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Addresses](
	[AddressId] [bigint] NOT NULL,
	[SourceType] [nvarchar](50) NOT NULL,
	[SourceId] [bigint] NOT NULL,
	[AddressType] [nvarchar](50) NOT NULL,
	[AddressLine1] [nvarchar](500) NULL,
	[AddressLine2] [nvarchar](500) NULL,
	[City] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](50) NULL,
	[CountryId] [int] NULL,
	[Phone] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Web] [nvarchar](50) NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppFunctionality]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppFunctionality](
	[Id] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[ModuleId] [int] NOT NULL,
	[Functionality] [nvarchar](250) NULL,
	[Url] [nvarchar](250) NULL,
	[ParentId] [int] NULL,
	[IsCompanySpecific] [bit] NULL,
	[IsActive] [bit] NULL,
	[Sequence] [int] NULL,
	[LastUpdated] [timestamp] NOT NULL,
	[Icon] [nvarchar](100) NULL,
 CONSTRAINT [PK_AppFunctionality] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppModule]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppModule](
	[Id] [int] NOT NULL,
	[Module] [nvarchar](50) NOT NULL,
	[CompanyId] [int] NULL,
	[AdminId] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AppPermission]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppPermission](
	[Id] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserId] [int] NULL,
	[CompanyId] [int] NULL,
	[FunctionalityId] [int] NULL,
	[IsView] [bit] NULL,
	[IsInsert] [bit] NULL,
	[IsUpdate] [bit] NULL,
	[IsDelete] [bit] NULL,
	[IsApprove] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Bank]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bank](
	[BankId] [int] NOT NULL,
	[BankCode] [nvarchar](50) NOT NULL,
	[BankName] [nvarchar](500) NOT NULL,
	[ContactPerson] [nvarchar](250) NULL,
	[ContactDesignation] [nvarchar](150) NULL,
	[ContactNo] [nvarchar](500) NULL,
	[ContactEmail] [nvarchar](150) NULL,
	[CompanyId] [int] NULL,
	[IsActive] [bit] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Bank] PRIMARY KEY CLUSTERED 
(
	[BankId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BankAccounts]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BankAccounts](
	[BankAccountId] [bigint] NOT NULL,
	[BankId] [bigint] NOT NULL,
	[BranchName] [nvarchar](150) NULL,
	[AccountNo] [nvarchar](50) NULL,
	[AccountTitle] [nvarchar](250) NULL,
	[AccountType] [nvarchar](150) NULL,
	[OpeningDate] [date] NULL,
	[CompanyId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_BankAccounts] PRIMARY KEY CLUSTERED 
(
	[BankAccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChartOfAccount]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChartOfAccount](
	[CoaId] [int] NOT NULL,
	[CoaType] [nvarchar](50) NOT NULL,
	[CoaGroupId] [int] NOT NULL,
	[CoaCode] [nvarchar](50) NOT NULL,
	[CoaTitle] [nvarchar](350) NOT NULL,
	[ParentId] [int] NULL,
	[IsActive] [bit] NULL,
	[UpdateBy] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_ChartOfAccount] PRIMARY KEY CLUSTERED 
(
	[CoaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChartOfAccountGroup]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChartOfAccountGroup](
	[CoaGroupId] [int] NOT NULL,
	[CoaGroupName] [nvarchar](150) NOT NULL,
	[ParentId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[UpdateBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[CompanyId] [int] NULL,
 CONSTRAINT [PK_CoaGroup] PRIMARY KEY CLUSTERED 
(
	[CoaGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Company]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](250) NOT NULL,
	[Address] [nvarchar](500) NULL,
	[Phone] [nvarchar](150) NULL,
	[Email] [nvarchar](50) NULL,
	[Web] [nvarchar](50) NULL,
	[LogoPath] [nvarchar](250) NULL,
	[UpdateBy] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[ContactId] [bigint] NOT NULL,
	[SourceType] [nvarchar](50) NOT NULL,
	[SourceId] [bigint] NOT NULL,
	[ContactName] [nvarchar](500) NULL,
	[ContactDesignation] [nvarchar](150) NULL,
	[IsMainContact] [bit] NULL,
	[Phone] [nvarchar](250) NULL,
	[Mobile] [nvarchar](250) NULL,
	[Email] [nvarchar](250) NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CostCenter]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CostCenter](
	[CostCenterId] [int] NOT NULL,
	[CostCenterType] [nvarchar](50) NOT NULL,
	[CostCenterName] [nvarchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[UpdateBy] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CostCenter] PRIMARY KEY CLUSTERED 
(
	[CostCenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [bigint] NOT NULL,
	[CustomerName] [nvarchar](500) NOT NULL,
	[CustomerCategoryId] [int] NULL,
	[SalesPersonId] [bigint] NULL,
	[IsActive] [bit] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[CreditLimit] [numeric](18, 0) NULL,
	[UpdateBy] [bigint] NULL,
	[UpdateDate] [datetime] NULL,
	[TotalDebit] [numeric](18, 4) NULL,
	[TotalCredit] [numeric](18, 4) NULL,
	[Balance]  AS ([TotalDebit]-[TotalCredit]),
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Department]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Department](
	[DepartmentId] [int] NOT NULL,
	[DepartmentName] [nvarchar](150) NOT NULL,
	[ParentDepartmentId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[UpdateBy] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[CompanyId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Designation]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Designation](
	[DesignationId] [int] NOT NULL,
	[Designation] [nvarchar](150) NOT NULL,
	[DepartmentId] [int] NULL,
	[CompanyId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UpdateBy] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeId] [int] NOT NULL,
	[EmployeeCode] [nvarchar](20) NULL,
	[EmployeeName] [nvarchar](520) NULL,
	[Address] [nvarchar](500) NULL,
	[DOB] [date] NULL,
	[JoinDate] [date] NULL,
	[DepartmentId] [int] NULL,
	[DesignationId] [int] NULL,
	[CompanyId] [int] NULL,
	[IsActive] [bit] NULL,
	[UpdateBy] [int] NULL,
	[UpdateDate] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[JournalDetails]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JournalDetails](
	[JournalDetailsId] [bigint] NOT NULL,
	[MasterId] [bigint] NOT NULL,
	[AccountNo] [bigint] NOT NULL,
	[Debit] [decimal](18, 4) NOT NULL,
	[Credit] [decimal](18, 4) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[CompanyId] [int] NOT NULL,
	[VoucherNo] [varchar](50) NULL,
 CONSTRAINT [PK_JournalDetails] PRIMARY KEY CLUSTERED 
(
	[JournalDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JournalMaster]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JournalMaster](
	[JournalId] [bigint] NOT NULL,
	[JournalDate] [date] NOT NULL,
	[JournalType] [nvarchar](50) NULL,
	[JournalDescription] [nvarchar](500) NULL,
	[UpdateBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[ApprovedBy] [int] NULL,
	[ApprovedDate] [datetime] NULL,
	[CompanyId] [int] NOT NULL,
	[PostDate] [datetime] NULL,
	[CostCenterId] [int] NULL,
 CONSTRAINT [PK_JournalMaster] PRIMARY KEY CLUSTERED 
(
	[JournalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ledger]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ledger](
	[LedgerId] [int] NOT NULL,
	[TransactionNo] [nvarchar](100) NULL,
	[TransactionDate] [datetime] NULL,
	[VoucherType] [nvarchar](50) NULL,
	[VoucherId] [bigint] NULL,
	[AccountNo] [nvarchar](50) NULL,
	[ContraAccountNo] [nvarchar](50) NULL,
	[Debit] [decimal](18, 0) NULL,
	[Credit] [decimal](18, 0) NULL,
	[Description] [nvarchar](100) NULL,
	[UserId] [int] NULL,
	[LastUpdateTime] [datetime] NULL,
	[CompanyId] [bigint] NULL,
 CONSTRAINT [PK__Ledger__AE70E0CFB44A3DE9] PRIMARY KEY CLUSTERED 
(
	[LedgerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ListTable]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListTable](
	[Id] [int] NOT NULL,
	[ListType] [nvarchar](50) NOT NULL,
	[ListId] [int] NOT NULL,
	[ListValue] [nvarchar](150) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UpdateBy] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier](
	[SupplierId] [bigint] NOT NULL,
	[SupplierName] [nvarchar](500) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[UpdateBy] [bigint] NULL,
	[UpdateDate] [datetime] NULL,
	[TotalDebit] [numeric](18, 4) NULL,
	[TotalCredit] [numeric](18, 4) NULL,
	[Balance]  AS ([TotalDebit]-[TotalCredit]),
 CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED 
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TempLedgerReportTable]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempLedgerReportTable](
	[VoucherTempId] [int] NOT NULL,
	[VoucherTrasactionDate] [date] NOT NULL,
	[MasterAccount] [varchar](50) NOT NULL,
	[MasterAccountCOAid] [int] NOT NULL,
	[ContraAccount] [varchar](50) NOT NULL,
	[ContraAccountId] [int] NOT NULL,
	[OpeningBalance] [decimal](18, 2) NULL,
	[Debit] [decimal](18, 2) NULL,
	[Credit] [decimal](18, 2) NULL,
	[ClosingBalance] [decimal](18, 2) NULL,
	[VoucherType] [varchar](50) NOT NULL,
	[TransactionCode] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TempLedgerReportTable_1] PRIMARY KEY CLUSTERED 
(
	[VoucherTempId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[RoleId] [int] NOT NULL,
	[RoleName] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UpdateBy] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRoleMapping]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoleMapping](
	[MappingId] [bigint] NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_UserRoleMapping] PRIMARY KEY CLUSTERED 
(
	[MappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[UserPassword] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UpdateBy] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[EmployeeId] [int] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VoucherChequeInfo]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoucherChequeInfo](
	[ChequeId] [bigint] NOT NULL,
	[VoucherType] [nvarchar](50) NOT NULL,
	[VoucherId] [bigint] NOT NULL,
	[BankId] [int] NULL,
	[BankName] [nvarchar](50) NULL,
	[Branch] [nvarchar](50) NULL,
	[AccountNo] [nvarchar](50) NULL,
	[ChequeNo] [nvarchar](50) NULL,
	[ChequeType] [nvarchar](50) NULL,
	[ChequeAmount] [numeric](18, 2) NULL,
	[IsPostToLedger] [bit] NULL,
	[SubmiteDate] [date] NULL,
	[SubmiteToBank] [int] NULL,
 CONSTRAINT [PK_VoucherChequeInfo] PRIMARY KEY CLUSTERED 
(
	[ChequeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VoucherDetail]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoucherDetail](
	[VoucherDetailId] [bigint] NOT NULL,
	[VoucherId] [bigint] NOT NULL,
	[COAId] [int] NOT NULL,
	[LineDesciption] [nvarchar](500) NULL,
	[Amount] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_VoucherDetail] PRIMARY KEY CLUSTERED 
(
	[VoucherDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VoucherDetailTemp]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VoucherDetailTemp](
	[Id] [int] NOT NULL,
	[userId] [bigint] NOT NULL,
	[COAId] [int] NOT NULL,
	[LineDesciption] [varchar](50) NOT NULL,
	[Amount] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_VoucherDetailTemp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VoucherMaster]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoucherMaster](
	[VoucherId] [bigint] NOT NULL,
	[VoucherDate] [date] NULL,
	[VoucherNo]  AS ('VOU-'+right((('0000'+CONVERT([varchar](10),[VoucherId]))+'-')+datename(year,getdate()),(10))),
	[VoucherType] [nvarchar](50) NULL,
	[PaymentMode] [nvarchar](50) NULL,
	[VoucherDescription] [nvarchar](500) NULL,
	[UpdateBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[ApprovedBy] [int] NULL,
	[ApprovedDate] [datetime] NULL,
	[CompanyId] [int] NOT NULL,
	[PostDate] [datetime] NULL,
	[CostCenterId] [int] NULL,
	[SourceCOAId] [int] NULL,
	[VoucherTotal] [nchar](10) NULL,
	[PaymentRef] [nvarchar](50) NULL,
	[IsPosted] [bit] NULL,
	[Status] [nvarchar](20) NULL,
	[SourceOfAccount] [nvarchar](50) NULL,
 CONSTRAINT [PK_VoucherMaster] PRIMARY KEY CLUSTERED 
(
	[VoucherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[AddressToCustomerView]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AddressToCustomerView]
AS
SELECT        dbo.Addresses.SourceId, dbo.Customer.CustomerId, dbo.Addresses.SourceType
FROM            dbo.Addresses INNER JOIN
                         dbo.Customer ON dbo.Addresses.SourceId = dbo.Customer.CustomerId
WHERE        (dbo.Addresses.SourceType = N'Customer')

GO
/****** Object:  View [dbo].[vewAppFunctionalityMenu]    Script Date: 12/24/2015 7:21:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vewAppFunctionalityMenu]
AS
SELECT        TOP (100) PERCENT dbo.AppPermission.Id, dbo.AppPermission.RoleId, dbo.AppPermission.UserId, dbo.AppPermission.CompanyId, dbo.AppFunctionality.Functionality, dbo.AppFunctionality.Url, 
                         dbo.AppFunctionality.IsActive, dbo.AppFunctionality.Sequence, dbo.AppPermission.FunctionalityId, dbo.AppFunctionality.ModuleId, dbo.AppModule.Module, dbo.AppFunctionality.ParentId, 
                         dbo.AppFunctionality.IsCompanySpecific, dbo.AppPermission.IsView, dbo.AppPermission.IsInsert, dbo.AppPermission.IsUpdate, dbo.AppPermission.IsDelete, dbo.AppPermission.IsApprove
FROM            dbo.AppModule INNER JOIN
                         dbo.AppFunctionality ON dbo.AppModule.Id = dbo.AppFunctionality.ModuleId INNER JOIN
                         dbo.AppPermission ON dbo.AppFunctionality.Id = dbo.AppPermission.FunctionalityId
ORDER BY dbo.AppFunctionality.ModuleId, dbo.AppFunctionality.Sequence








GO
INSERT [dbo].[Addresses] ([AddressId], [SourceType], [SourceId], [AddressType], [AddressLine1], [AddressLine2], [City], [ZipCode], [CountryId], [Phone], [Mobile], [Email], [Web], [CompanyId]) VALUES (3, N'Customer', 1, N'Main Address', N'Gazipur Industrial Area', N'Savar EPZ', N'Dhaka', N'1254', 9, N'5142780', N'0124154163', N'incepta@mail.com', N'www.incepta.pharm.bd', 1010)
INSERT [dbo].[Addresses] ([AddressId], [SourceType], [SourceId], [AddressType], [AddressLine1], [AddressLine2], [City], [ZipCode], [CountryId], [Phone], [Mobile], [Email], [Web], [CompanyId]) VALUES (4, N'Company', 1010, N'Main Address', N'Dhaka', N'Dhaka', N'Dhaka', N'1208', 9, N'+8801212', N'122333', N'info@Drugland.Com', N'druglandbd.com', 1010)
INSERT [dbo].[Addresses] ([AddressId], [SourceType], [SourceId], [AddressType], [AddressLine1], [AddressLine2], [City], [ZipCode], [CountryId], [Phone], [Mobile], [Email], [Web], [CompanyId]) VALUES (5, N'Company', 1013, N'Main Address', N'Dhaka', N'Rajbari', N'Comilla', N'1033', 9, N'+88012121', N'+88012121', N'info@Hubfrieght.com', N'hubfreight.com', 0)
INSERT [dbo].[Addresses] ([AddressId], [SourceType], [SourceId], [AddressType], [AddressLine1], [AddressLine2], [City], [ZipCode], [CountryId], [Phone], [Mobile], [Email], [Web], [CompanyId]) VALUES (6, N'Company', 1014, N'Main Address', N'Dhaka', N'Dhaka', N'Dhaka', N'1033', 9, N'+88012121', N'+88012121', N'info@Hubfrieght.com', N'hubfreight.com', 0)
INSERT [dbo].[Addresses] ([AddressId], [SourceType], [SourceId], [AddressType], [AddressLine1], [AddressLine2], [City], [ZipCode], [CountryId], [Phone], [Mobile], [Email], [Web], [CompanyId]) VALUES (7, N'Employee', 1, N'Main Address', N'Marine', N'Kotbari', N'Dhaka', N'1254', 9, N'5142780', N'', N'glaxo@mail.com', N'', 1010)
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (10, 1, 1, N'Accounts', N'#', 0, 1, 1, 0, N'fa fa-edit')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (11, 1, 1, N'Journal', N'JournalInformationDetail.aspx', 10, 1, 1, 1, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (12, 1, 3, N'Journal Lists', N'JournalMasterList.aspx', 10, 1, 1, 2, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (20, 1, 4, N'Setup', N'#', 0, 1, 1, 0, N'fa fa-edit')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (21, 1, 4, N'Company List', N'CompanyList.aspx', 20, 1, 1, 1, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (22, 1, 4, N'Chart Of Account Group List', N'ChartOfAccountList.aspx', 20, 1, 1, 2, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (23, 1, 4, N'Chart of Account List', N'ChartOfAccountingInfoList.aspx', 20, 1, 1, 3, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (24, 1, 4, N'Bank List', N'BankLists.aspx', 20, 1, 1, 4, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (25, 1, 4, N'Bank Account List', N'BankAccoutsList.aspx', 20, 1, 1, 5, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (26, 1, 4, N' Cost Center List', N'CostCenterList.aspx', 20, 1, 1, 6, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (27, 1, 4, N'Customer List', N'CustomerList.aspx', 20, 1, 1, 7, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (28, 1, 4, N'Department List', N'DepartmentLists.aspx', 20, 1, 1, 8, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (30, 1, 4, N'Designation List', N'DesignationList.aspx', 20, 1, 1, 10, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (31, 1, 4, N'Employee List', N'EmployeeList.aspx', 20, 1, 1, 11, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (32, 1, 4, N'Supplier List', N'SupplierList.aspx', 20, 1, 1, 12, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (33, 1, 4, N'User Info', N'UserInfo.aspx', 20, 1, 1, 13, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (34, 1, 4, N'List Table List', N'ListTableLists.aspx', 20, 1, 1, 14, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (35, 1, 5, N'Voucher', N'#', 0, 1, 1, 0, N'fa fa-edit')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (36, 1, 5, N'Recieve Voucher', N'VoucherInformation.aspx', 35, 1, 1, 1, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (37, 1, 5, N'Voucher Lists', N'VoucherLists.aspx', 35, 1, 1, 2, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (38, 1, 5, N'Payment Voucher', N'VoucherPayment.aspx', 35, 1, 1, 3, N'fa fa-circle-o')
INSERT [dbo].[AppFunctionality] ([Id], [CompanyId], [ModuleId], [Functionality], [Url], [ParentId], [IsCompanySpecific], [IsActive], [Sequence], [Icon]) VALUES (39, 1, 5, N'Cheque Lists', N'ChequeList.aspx', 35, 1, 1, 4, N'fa fa-circle-o')
INSERT [dbo].[AppModule] ([Id], [Module], [CompanyId], [AdminId]) VALUES (1, N'Accounts', 1, 1)
INSERT [dbo].[AppModule] ([Id], [Module], [CompanyId], [AdminId]) VALUES (2, N'Customer', 1, 1)
INSERT [dbo].[AppModule] ([Id], [Module], [CompanyId], [AdminId]) VALUES (3, N'Supplier', 1, 1)
INSERT [dbo].[AppModule] ([Id], [Module], [CompanyId], [AdminId]) VALUES (4, N'Setup', 1, 1)
INSERT [dbo].[AppModule] ([Id], [Module], [CompanyId], [AdminId]) VALUES (5, N'Banks', 1, 1)
INSERT [dbo].[AppModule] ([Id], [Module], [CompanyId], [AdminId]) VALUES (6, N'User Management', 1, 1)
INSERT [dbo].[AppModule] ([Id], [Module], [CompanyId], [AdminId]) VALUES (7, N'Report', 1, 1)
INSERT [dbo].[AppModule] ([Id], [Module], [CompanyId], [AdminId]) VALUES (8, N'Bank Info', 0, 0)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (1, 1, 0, 0, 10, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (2, 1, 0, 0, 11, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (3, 1, 0, 0, 12, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (4, 1, 0, 0, 20, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (5, 1, 0, 0, 21, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (6, 1, 0, 0, 6, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (7, 1, 0, 0, 22, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (8, 1, 0, 0, 23, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (9, 1, 0, 0, 24, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (10, 1, 0, 0, 25, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (11, 1, 0, 0, 26, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (12, 1, 0, 0, 27, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (13, 1, 0, 0, 28, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (14, 1, 0, 0, 29, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (15, 1, 0, 0, 30, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (16, 1, 0, 0, 31, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (17, 1, 0, 0, 32, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (18, 1, 0, 0, 33, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (19, 1, 0, 0, 34, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (20, 1, 0, 0, 35, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (21, 1, 0, 0, 36, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (22, 1, 0, 0, 37, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (23, 1, 0, 0, 38, 1, 1, 1, 1, 1)
INSERT [dbo].[AppPermission] ([Id], [RoleId], [UserId], [CompanyId], [FunctionalityId], [IsView], [IsInsert], [IsUpdate], [IsDelete], [IsApprove]) VALUES (24, 1, 0, 0, 39, 1, 1, 1, 1, 1)
INSERT [dbo].[Bank] ([BankId], [BankCode], [BankName], [ContactPerson], [ContactDesignation], [ContactNo], [ContactEmail], [CompanyId], [IsActive], [UpdatedBy], [UpdatedDate]) VALUES (1, N'ibbl', N'Islami Bank Bangladesh Limited', N'Mr. Mazid', N'CEO', N'02-8745213', N'mazid@ibbl.bd.com', 1010, 1, 1, CAST(0x0000A55B00000000 AS DateTime))
INSERT [dbo].[Bank] ([BankId], [BankCode], [BankName], [ContactPerson], [ContactDesignation], [ContactNo], [ContactEmail], [CompanyId], [IsActive], [UpdatedBy], [UpdatedDate]) VALUES (2, N'1418', N'Duthc Bangla Bank Limited', N'Hayat', N'Assistant Manager', N'015554544545', N'hayat@gmail.com', 1013, 1, 1, CAST(0x0000A55E00000000 AS DateTime))
INSERT [dbo].[BankAccounts] ([BankAccountId], [BankId], [BranchName], [AccountNo], [AccountTitle], [AccountType], [OpeningDate], [CompanyId], [IsActive]) VALUES (1, 1, N'Gulshan', N'ibbl/11/15/01', N'Temporary Drugland Account', N'Marchent', CAST(0xB53A0B00 AS Date), 1010, 1)
INSERT [dbo].[ChartOfAccount] ([CoaId], [CoaType], [CoaGroupId], [CoaCode], [CoaTitle], [ParentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (1, N'Marchent', 1, N'coa/11/15/2015', N'my title', 1, 1, 1, CAST(0x0000A55B011AF774 AS DateTime), 1010)
INSERT [dbo].[ChartOfAccount] ([CoaId], [CoaType], [CoaGroupId], [CoaCode], [CoaTitle], [ParentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (2, N'Cash', 1, N'1010', N'Cash', 2, 1, 1, CAST(0x0000A57800DDD18C AS DateTime), 1010)
INSERT [dbo].[ChartOfAccount] ([CoaId], [CoaType], [CoaGroupId], [CoaCode], [CoaTitle], [ParentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (3, N'Conveyance', 2, N'2010', N'Conveyance', 3, 1, 1, CAST(0x0000A57800DDEDAC AS DateTime), 1010)
INSERT [dbo].[ChartOfAccount] ([CoaId], [CoaType], [CoaGroupId], [CoaCode], [CoaTitle], [ParentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (4, N'Entertainment', 2, N'2011', N'Entertainment', 4, 1, 1, CAST(0x0000A57800DE051C AS DateTime), 1010)
INSERT [dbo].[ChartOfAccount] ([CoaId], [CoaType], [CoaGroupId], [CoaCode], [CoaTitle], [ParentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (5, N'Sales', 1, N'2013', N'Sales', 5, 1, 1, CAST(0x0000A57800DE7A4C AS DateTime), 1010)
INSERT [dbo].[ChartOfAccount] ([CoaId], [CoaType], [CoaGroupId], [CoaCode], [CoaTitle], [ParentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (6, N'Service', 1, N'2014', N'Service', 6, 1, 1, CAST(0x0000A57800DECEAC AS DateTime), 1010)
INSERT [dbo].[ChartOfAccount] ([CoaId], [CoaType], [CoaGroupId], [CoaCode], [CoaTitle], [ParentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (7, N'Bank', 1, N'3011', N'Bank', 7, 1, 1, CAST(0x0000A57800000000 AS DateTime), 1010)
INSERT [dbo].[ChartOfAccountGroup] ([CoaGroupId], [CoaGroupName], [ParentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (1, N'Asset', 1, 1, 1, CAST(0x0000A57800DD36DC AS DateTime), 1010)
INSERT [dbo].[ChartOfAccountGroup] ([CoaGroupId], [CoaGroupName], [ParentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (2, N'Expense', 1, 1, 1, CAST(0x0000A57800DD44EC AS DateTime), 1010)
SET IDENTITY_INSERT [dbo].[Company] ON 

INSERT [dbo].[Company] ([CompanyId], [CompanyName], [Address], [Phone], [Email], [Web], [LogoPath], [UpdateBy], [UpdateDate], [IsActive]) VALUES (1010, N'DrugLand', N'Dhaka', N'01670121212', N'info@drugland', N'www.drugland.bd', N'www.drugland.bd/logo.html', 1, CAST(0x0000A55100000000 AS DateTime), 1)
INSERT [dbo].[Company] ([CompanyId], [CompanyName], [Address], [Phone], [Email], [Web], [LogoPath], [UpdateBy], [UpdateDate], [IsActive]) VALUES (1013, N'Hub Frieght', N'Dhaka', N'015564644545', N'info@hubfreight.com', N'www.hubfreight.com', N'www.hubfrieght.com/logo.html', 1, CAST(0x0000A55C00DE8604 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Company] OFF
INSERT [dbo].[Contacts] ([ContactId], [SourceType], [SourceId], [ContactName], [ContactDesignation], [IsMainContact], [Phone], [Mobile], [Email], [CompanyId]) VALUES (0, N'Name', 1, N'Mr Hawlader', N'1', 0, N'123123', N'123123', N'mdasdas@asdmasd.com', 0)
INSERT [dbo].[CostCenter] ([CostCenterId], [CostCenterType], [CostCenterName], [IsActive], [CompanyId], [UpdateBy], [UpdateDate]) VALUES (1, N'temporary', N'drugland office edit', 1, 1010, 1, CAST(0x0000A55B00000000 AS DateTime))
INSERT [dbo].[CostCenter] ([CostCenterId], [CostCenterType], [CostCenterName], [IsActive], [CompanyId], [UpdateBy], [UpdateDate]) VALUES (2, N'Asset', N'Foundation work', 1, 1013, 1, CAST(0x0000A55E00000000 AS DateTime))
INSERT [dbo].[Customer] ([CustomerId], [CustomerName], [CustomerCategoryId], [SalesPersonId], [IsActive], [CompanyId], [CreditLimit], [UpdateBy], [UpdateDate], [TotalDebit], [TotalCredit]) VALUES (1, N'omi', 1, 0, 1, 1010, CAST(1000 AS Numeric(18, 0)), 1, CAST(0x0000A55B0135FAB0 AS DateTime), CAST(0.0000 AS Numeric(18, 4)), CAST(0.0000 AS Numeric(18, 4)))
INSERT [dbo].[Department] ([DepartmentId], [DepartmentName], [ParentDepartmentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (1, N'test dept', 1, 1, 1, CAST(0x0000A55B01357770 AS DateTime), 1010)
INSERT [dbo].[Department] ([DepartmentId], [DepartmentName], [ParentDepartmentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (2, N'Testing Unit', 2, 1, 1, CAST(0x0000A55E00EA3954 AS DateTime), 1013)
INSERT [dbo].[Department] ([DepartmentId], [DepartmentName], [ParentDepartmentId], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (3, N'Testing Unit 2', 3, 1, 1, CAST(0x0000A55E00EA8EE0 AS DateTime), 1013)
INSERT [dbo].[Designation] ([DesignationId], [Designation], [DepartmentId], [CompanyId], [IsActive], [UpdateBy], [UpdateDate]) VALUES (1, N'Manager', 1, 1, 1, 1, CAST(0x0000A52400000000 AS DateTime))
INSERT [dbo].[Designation] ([DesignationId], [Designation], [DepartmentId], [CompanyId], [IsActive], [UpdateBy], [UpdateDate]) VALUES (2, N'Accountant', 1, 1010, 1, 1, CAST(0x0000A56600B883F0 AS DateTime))
INSERT [dbo].[Employee] ([EmployeeId], [EmployeeCode], [EmployeeName], [Address], [DOB], [JoinDate], [DepartmentId], [DesignationId], [CompanyId], [IsActive], [UpdateBy], [UpdateDate]) VALUES (1, N'101', N'Faruk Ahmed', N'Main Address', CAST(0xA73A0B00 AS Date), CAST(0xA03A0B00 AS Date), 1, 1, 1010, 1, 1, CAST(0x0000A55C00EB503C AS DateTime))
INSERT [dbo].[JournalDetails] ([JournalDetailsId], [MasterId], [AccountNo], [Debit], [Credit], [Description], [CompanyId], [VoucherNo]) VALUES (1, 1, 0, CAST(1000.0000 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), N'test', 0, N'JV-1-2015')
INSERT [dbo].[JournalDetails] ([JournalDetailsId], [MasterId], [AccountNo], [Debit], [Credit], [Description], [CompanyId], [VoucherNo]) VALUES (2, 1, 0, CAST(0.0000 AS Decimal(18, 4)), CAST(1000.0000 AS Decimal(18, 4)), N'test', 0, N'JV-2-2015')
INSERT [dbo].[JournalMaster] ([JournalId], [JournalDate], [JournalType], [JournalDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId]) VALUES (1, CAST(0xA03A0B00 AS Date), N'Document', N'test', 1, CAST(0x0000A54D00AFAF64 AS DateTime), NULL, NULL, 1, NULL, 0)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (1, N'VOU-00003-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 3, N'4', N'7', CAST(0 AS Decimal(18, 0)), CAST(1200 AS Decimal(18, 0)), N'N/A', 1, CAST(0x0000A5780126B67C AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (2, N'VOU-00003-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 3, N'7', N'4', CAST(1200 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'N/A', 1, CAST(0x0000A5780126B67C AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (3, N'VOU-00003-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 3, N'5', N'7', CAST(0 AS Decimal(18, 0)), CAST(1800 AS Decimal(18, 0)), N'N/A', 1, CAST(0x0000A5780126B67C AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (4, N'VOU-00003-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 3, N'7', N'5', CAST(1800 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'N/A', 1, CAST(0x0000A5780126B67C AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (5, N'VOU-00003-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 3, N'6', N'7', CAST(0 AS Decimal(18, 0)), CAST(800 AS Decimal(18, 0)), N'N/A', 1, CAST(0x0000A5780126B67C AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (6, N'VOU-00003-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 3, N'7', N'6', CAST(800 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'N/A', 1, CAST(0x0000A5780126B67C AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (7, N'VOU-00004-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 4, N'6', N'2', CAST(0 AS Decimal(18, 0)), CAST(1600 AS Decimal(18, 0)), N'fads', 1, CAST(0x0000A5780126B67C AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (8, N'VOU-00004-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 4, N'2', N'6', CAST(1600 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'fads', 1, CAST(0x0000A5780126B67C AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (9, N'VOU-00004-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 4, N'5', N'2', CAST(0 AS Decimal(18, 0)), CAST(2200 AS Decimal(18, 0)), N'fads', 1, CAST(0x0000A5780126B67C AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (10, N'VOU-00004-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 4, N'2', N'5', CAST(2200 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'fads', 1, CAST(0x0000A5780126B67C AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (11, N'VOU-00011-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 11, N'1', N'7', CAST(0 AS Decimal(18, 0)), CAST(1200 AS Decimal(18, 0)), N'recieve voucher', 1, CAST(0x0000A5780126B7A8 AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (12, N'VOU-00011-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 11, N'7', N'1', CAST(1200 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'recieve voucher', 1, CAST(0x0000A5780126B7A8 AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (13, N'VOU-00011-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 11, N'6', N'7', CAST(0 AS Decimal(18, 0)), CAST(12000 AS Decimal(18, 0)), N'recieve voucher', 1, CAST(0x0000A5780126B7A8 AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (14, N'VOU-00011-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 11, N'7', N'6', CAST(12000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'recieve voucher', 1, CAST(0x0000A5780126B7A8 AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (15, N'VOU-00011-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 11, N'3', N'7', CAST(0 AS Decimal(18, 0)), CAST(500 AS Decimal(18, 0)), N'recieve voucher', 1, CAST(0x0000A5780126B7A8 AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (16, N'VOU-00011-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 11, N'7', N'3', CAST(500 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'recieve voucher', 1, CAST(0x0000A5780126B7A8 AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (17, N'VOU-00017-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 17, N'5', N'2', CAST(0 AS Decimal(18, 0)), CAST(1000 AS Decimal(18, 0)), N'Recepit Voucher', 1, CAST(0x0000A5780126B7A8 AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (18, N'VOU-00017-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 17, N'2', N'5', CAST(1000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'Recepit Voucher', 1, CAST(0x0000A5780126B7A8 AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (19, N'VOU-00017-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 17, N'6', N'2', CAST(0 AS Decimal(18, 0)), CAST(1500 AS Decimal(18, 0)), N'Recepit Voucher', 1, CAST(0x0000A5780126B8D4 AS DateTime), 1010)
INSERT [dbo].[Ledger] ([LedgerId], [TransactionNo], [TransactionDate], [VoucherType], [VoucherId], [AccountNo], [ContraAccountNo], [Debit], [Credit], [Description], [UserId], [LastUpdateTime], [CompanyId]) VALUES (20, N'VOU-00017-2015', CAST(0x0000A56100000000 AS DateTime), N'Receipt', 17, N'2', N'6', CAST(1500 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'Recepit Voucher', 1, CAST(0x0000A5780126B8D4 AS DateTime), 1010)
INSERT [dbo].[ListTable] ([Id], [ListType], [ListId], [ListValue], [CompanyId], [IsActive], [UpdateBy], [UpdateDate]) VALUES (0, N'Journal', 1, N'Invoice', 1, 1, 1, CAST(0x0000A53C00000000 AS DateTime))
INSERT [dbo].[ListTable] ([Id], [ListType], [ListId], [ListValue], [CompanyId], [IsActive], [UpdateBy], [UpdateDate]) VALUES (1, N'Document', 1, N'12', 0, 1, 1, CAST(0x0000A5440133C4AC AS DateTime))
INSERT [dbo].[ListTable] ([Id], [ListType], [ListId], [ListValue], [CompanyId], [IsActive], [UpdateBy], [UpdateDate]) VALUES (0, N'Journal New', 2, N'22', 0, 1, 1, CAST(0x0000A5440134E3C8 AS DateTime))
INSERT [dbo].[ListTable] ([Id], [ListType], [ListId], [ListValue], [CompanyId], [IsActive], [UpdateBy], [UpdateDate]) VALUES (2, N'test type', 1, N'test value', 1010, 1, 1, CAST(0x0000A55B012E4EA0 AS DateTime))
INSERT [dbo].[ListTable] ([Id], [ListType], [ListId], [ListValue], [CompanyId], [IsActive], [UpdateBy], [UpdateDate]) VALUES (3, N'Journal', 4, N'Invoice edit', 1010, 1, 1, CAST(0x0000A55B01308CD8 AS DateTime))
INSERT [dbo].[Supplier] ([SupplierId], [SupplierName], [IsActive], [CompanyId], [UpdateBy], [UpdateDate], [TotalDebit], [TotalCredit]) VALUES (1, N'Incepta', 1, 1, 1, CAST(0x0000A54500D524D8 AS DateTime), CAST(1000.0000 AS Numeric(18, 4)), CAST(1208.0000 AS Numeric(18, 4)))
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (1, CAST(0xBD3A0B00 AS Date), N'Bank', 7, N'Entertainment', 4, CAST(0.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00003-2015')
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (2, CAST(0xBD3A0B00 AS Date), N'Bank', 7, N'Entertainment', 4, CAST(1200.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(2400.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00003-2015')
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (3, CAST(0xBD3A0B00 AS Date), N'Bank', 7, N'Sales', 5, CAST(2400.00 AS Decimal(18, 2)), CAST(1800.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4200.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00003-2015')
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (4, CAST(0xBD3A0B00 AS Date), N'Bank', 7, N'Service', 6, CAST(4200.00 AS Decimal(18, 2)), CAST(800.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00003-2015')
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (5, CAST(0xBC3A0B00 AS Date), N'Cash', 2, N'Service', 6, CAST(0.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1600.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00004-2015')
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (6, CAST(0xBC3A0B00 AS Date), N'Cash', 2, N'Sales', 5, CAST(1600.00 AS Decimal(18, 2)), CAST(2200.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(3800.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00004-2015')
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (7, CAST(0xBC3A0B00 AS Date), N'Bank', 7, N'my title', 1, CAST(5000.00 AS Decimal(18, 2)), CAST(1200.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(6200.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00011-2015')
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (8, CAST(0xBC3A0B00 AS Date), N'Bank', 7, N'Service', 6, CAST(6200.00 AS Decimal(18, 2)), CAST(12000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(18200.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00011-2015')
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (9, CAST(0xBC3A0B00 AS Date), N'Bank', 7, N'Conveyance', 3, CAST(18200.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(18700.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00011-2015')
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (10, CAST(0xBC3A0B00 AS Date), N'Cash', 2, N'Sales', 5, CAST(3800.00 AS Decimal(18, 2)), CAST(1000.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(4800.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00017-2015')
INSERT [dbo].[TempLedgerReportTable] ([VoucherTempId], [VoucherTrasactionDate], [MasterAccount], [MasterAccountCOAid], [ContraAccount], [ContraAccountId], [OpeningBalance], [Debit], [Credit], [ClosingBalance], [VoucherType], [TransactionCode]) VALUES (11, CAST(0xBC3A0B00 AS Date), N'Cash', 2, N'Service', 6, CAST(4800.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(6300.00 AS Decimal(18, 2)), N'Receipt', N'VOU-00017-2015')
INSERT [dbo].[UserRole] ([RoleId], [RoleName], [IsActive], [UpdateBy], [UpdateDate], [CompanyId]) VALUES (1, N'Super', 1, 1, CAST(0x0000A41300000000 AS DateTime), 0)
INSERT [dbo].[UserRoleMapping] ([MappingId], [UserId], [RoleId]) VALUES (1, 1, 1)
INSERT [dbo].[Users] ([UserId], [UserName], [UserPassword], [IsActive], [UpdateBy], [UpdateDate], [CompanyId], [EmployeeId]) VALUES (1, N'Super', N'pass12', 1, 1, CAST(0x0000A41300000000 AS DateTime), 0, 0)
INSERT [dbo].[VoucherChequeInfo] ([ChequeId], [VoucherType], [VoucherId], [BankId], [BankName], [Branch], [AccountNo], [ChequeNo], [ChequeType], [ChequeAmount], [IsPostToLedger], [SubmiteDate], [SubmiteToBank]) VALUES (1, N'Recieved Voucher', 5, 1, N'1', N'Corporate', N'12350055', N'12546885', N'Bearer', CAST(250.00 AS Numeric(18, 2)), 1, CAST(0xBC3A0B00 AS Date), 1)
INSERT [dbo].[VoucherChequeInfo] ([ChequeId], [VoucherType], [VoucherId], [BankId], [BankName], [Branch], [AccountNo], [ChequeNo], [ChequeType], [ChequeAmount], [IsPostToLedger], [SubmiteDate], [SubmiteToBank]) VALUES (2, N'Recieved Voucher', 6, 1, N'1', N'abccccc', N'12350055', N'12546885', N'Bearer', CAST(250.00 AS Numeric(18, 2)), 1, CAST(0xC93A0B00 AS Date), 1)
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (1, 1, 1, N'Recieved ', CAST(1000.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (2, 1, 1, N'Deposit', CAST(1200.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (5, 4, 6, N'updatedData', CAST(1600.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (6, 4, 5, N'testUpdate1', CAST(2200.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (8, 5, 1, N'Detiail Description 2', CAST(1250.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (9, 5, 1, N'Detiail Description 3', CAST(1250.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (10, 5, 1, N'Detiail Description 4', CAST(1250.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (11, 5, 1, N'Detiail Description 5', CAST(1250.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (13, 5, 1, N'Detiail Description 7', CAST(1250.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (15, 5, 1, N'Detiail Description 8', CAST(1250.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (16, 5, 1, N'Detiail Description 9', CAST(1250.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (17, 5, 1, N'Detiail Description 9', CAST(125000.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (18, 7, 1, N'dddd', CAST(12544.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (19, 9, 1, N'abc description', CAST(100.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (20, 10, 1, N'fldsajfl', CAST(525.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (21, 10, 1, N'hello', CAST(525.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (22, 10, 1, N'hello', CAST(2000.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (23, 11, 1, N'fdasf', CAST(1200.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (25, 13, 1, N'Get', CAST(1200.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (26, 13, 1, N'Get', CAST(1200.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (27, 13, 1, N'Get', CAST(1200.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (28, 14, 1, N'fasdf', CAST(5000.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (29, 14, 1, N'fasdf', CAST(5000.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (30, 14, 1, N'fasdf', CAST(5000.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (31, 14, 1, N'fasdf', CAST(5000.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (32, 15, 1, N'fsd', CAST(56.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (33, 16, 1, N'123123', CAST(11200.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (34, 16, 1, N'123123', CAST(11200.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (35, 16, 1, N'123123', CAST(11200.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (36, 16, 1, N'123123', CAST(10.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (37, 17, 5, N'Description', CAST(1000.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (38, 17, 6, N'Description', CAST(1500.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (39, 3, 4, N'Description', CAST(1200.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (40, 3, 5, N'De', CAST(1800.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (41, 3, 6, N'xx', CAST(800.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (42, 11, 6, N'vv', CAST(12000.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherDetail] ([VoucherDetailId], [VoucherId], [COAId], [LineDesciption], [Amount]) VALUES (43, 11, 3, N'zz', CAST(500.00 AS Numeric(18, 2)))
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (1, CAST(0xBC3A0B00 AS Date), N'Receipt', N'Cash', N'Recieve', 1, CAST(0x0000A56200C810CC AS DateTime), 1, CAST(0x0000A56200C810CC AS DateTime), 1010, CAST(0x0000A56200C810CC AS DateTime), 1, 1, N'12000     ', N'Bank Recieveable payment', 1, N'Checked', NULL)
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (2, CAST(0xC33A0B00 AS Date), N'Receipt', N'Cash', N'dsfg', 1, CAST(0x0000A567013F860C AS DateTime), 1, CAST(0x0000A567013F860C AS DateTime), 1010, CAST(0x0000A567013F860C AS DateTime), 1, 1, N'345       ', N'345', 1, N'Checked', NULL)
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (3, CAST(0xBD3A0B00 AS Date), N'Receipt', N'Bank', N'N/A', 1, CAST(0x0000A56D0110D3C0 AS DateTime), 1, CAST(0x0000A56D0110D3C0 AS DateTime), 1010, CAST(0x0000A56D0110D3C0 AS DateTime), 1, 7, N'500       ', N'pay slip', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (4, CAST(0xBC3A0B00 AS Date), N'Receipt', N'Cash', N'fads', 1, CAST(0x0000A56D011340D8 AS DateTime), 1, CAST(0x0000A56D011340D8 AS DateTime), 1010, CAST(0x0000A56D011340D8 AS DateTime), 1, 2, N'3800      ', N'asdf', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (5, CAST(0xBC3A0B00 AS Date), N'Receipt', N'Bank', N'Voucher Description', 1, CAST(0x0000A56E00BBA1FC AS DateTime), 1, CAST(0x0000A56E00BBA1FC AS DateTime), 1010, CAST(0x0000A56E00BBA1FC AS DateTime), 1, 0, N'500       ', N'ref1', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (6, CAST(0xBC3A0B00 AS Date), N'Expense', N'Bank', N'In Chequefdfdasfa', 1, CAST(0x0000A57500C1B060 AS DateTime), 1, CAST(0x0000A57500C1B060 AS DateTime), 1010, CAST(0x0000A57500C1B060 AS DateTime), 1, 1, N'125000    ', N'125450000', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (7, CAST(0xBC3A0B00 AS Date), N'Expense', N'Cash', N'Add to Cash', 1, CAST(0x0000A56E010A679C AS DateTime), 1, CAST(0x0000A56E010A679C AS DateTime), 1010, CAST(0x0000A56E010A679C AS DateTime), 1, 0, N'100       ', N'ref2', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (8, CAST(0xBC3A0B00 AS Date), N'Receipt', N'Bank', N'Cheque Recieve', 1, CAST(0x0000A56F00B468C4 AS DateTime), 1, CAST(0x0000A56F00B468C4 AS DateTime), 1010, CAST(0x0000A56F00B468C4 AS DateTime), 1, 0, N'10000     ', N'bank', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (9, CAST(0xBC3A0B00 AS Date), N'Recieve Voucher', N'Cash', N'Cash Voucher With Single Data', 1, CAST(0x0000A56F0102BDE4 AS DateTime), 1, CAST(0x0000A56F0102BDE4 AS DateTime), 1010, CAST(0x0000A56F0102BDE4 AS DateTime), 1, 0, N'5000      ', N'12545', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (10, CAST(0xBC3A0B00 AS Date), N'Recieve Voucher', N'Cash', N'afdsaflfj', 1, CAST(0x0000A56F010352B8 AS DateTime), 1, CAST(0x0000A56F010352B8 AS DateTime), 1010, CAST(0x0000A56F010352B8 AS DateTime), 1, 0, N'1000      ', N'final ', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (11, CAST(0xBC3A0B00 AS Date), N'Receipt', N'Bank', N'recieve voucher', 1, CAST(0x0000A56F01051E18 AS DateTime), 1, CAST(0x0000A56F01051E18 AS DateTime), 1010, CAST(0x0000A56F01051E18 AS DateTime), 1, 7, N'12500     ', N'fsadf', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (12, CAST(0xBD3A0B00 AS Date), N'Receipt', N'Cash', N'sdfsdfsdddd', 1, CAST(0x0000A57100C8AF00 AS DateTime), 1, CAST(0x0000A57100C8AF00 AS DateTime), 1010, CAST(0x0000A57100C8AF00 AS DateTime), 1, 0, N'1000      ', N'fffffffffff', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (13, CAST(0x5B950A00 AS Date), N'Recieve Voucher', N'Cash', N'sdfsdfsdddd', 1, CAST(0x0000A57100C8C79C AS DateTime), 1, CAST(0x0000A57100C8C79C AS DateTime), 1010, CAST(0x0000A57100C8C79C AS DateTime), 1, 0, N'1000      ', N'fffffffffff', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (14, CAST(0xBC3A0B00 AS Date), N'Recieve Voucher', N'Bank', N'fasd', 1, CAST(0x0000A57300BCB560 AS DateTime), 1, CAST(0x0000A57300BCB560 AS DateTime), 1010, CAST(0x0000A57300BCB560 AS DateTime), 1, 0, N'200       ', N'fdsaf', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (15, CAST(0xBC3A0B00 AS Date), N'Recieve Voucher', N'Cash', N'fsda', 1, CAST(0x0000A57300BFB74C AS DateTime), 1, CAST(0x0000A57300BFB74C AS DateTime), 1010, CAST(0x0000A57300BFB74C AS DateTime), 1, 0, N'100       ', N'fsad', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (16, CAST(0xBD3A0B00 AS Date), N'Recieve Voucher', N'Bank', N'156456', 1, CAST(0x0000A57300C24FC0 AS DateTime), 1, CAST(0x0000A57300C24FC0 AS DateTime), 1010, CAST(0x0000A57300C24FC0 AS DateTime), 1, 0, N'300       ', N'45645', 1, N'Checked', N'')
INSERT [dbo].[VoucherMaster] ([VoucherId], [VoucherDate], [VoucherType], [PaymentMode], [VoucherDescription], [UpdateBy], [UpdateDate], [ApprovedBy], [ApprovedDate], [CompanyId], [PostDate], [CostCenterId], [SourceCOAId], [VoucherTotal], [PaymentRef], [IsPosted], [Status], [SourceOfAccount]) VALUES (17, CAST(0xBC3A0B00 AS Date), N'Receipt', N'Cash', N'Recepit Voucher', 1, CAST(0x0000A57800DF6F38 AS DateTime), 1, CAST(0x0000A57800DF6F38 AS DateTime), 1010, CAST(0x0000A57800DF6F38 AS DateTime), 1, 2, N'2500.00   ', N'Cash', 1, N'Checked', N'Cash')
ALTER TABLE [dbo].[Ledger] ADD  CONSTRAINT [DF__Ledger__Transact__04E4BC85]  DEFAULT (getdate()) FOR [TransactionDate]
GO
ALTER TABLE [dbo].[TempLedgerReportTable] ADD  CONSTRAINT [DF_TempLedgerReportTable_OpeningBalance]  DEFAULT ((0)) FOR [OpeningBalance]
GO
ALTER TABLE [dbo].[TempLedgerReportTable] ADD  CONSTRAINT [DF_TempLedgerReportTable_Debit]  DEFAULT ((0)) FOR [Debit]
GO
ALTER TABLE [dbo].[TempLedgerReportTable] ADD  CONSTRAINT [DF_TempLedgerReportTable_Credit]  DEFAULT ((0)) FOR [Credit]
GO
ALTER TABLE [dbo].[TempLedgerReportTable] ADD  CONSTRAINT [DF_TempLedgerReportTable_ClosingBalance]  DEFAULT ((0)) FOR [ClosingBalance]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cash,Bank' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VoucherMaster', @level2type=N'COLUMN',@level2name=N'VoucherType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Debit,Credit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VoucherMaster', @level2type=N'COLUMN',@level2name=N'PaymentMode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Addresses"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Customer"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 445
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1845
         Or = 2775
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AddressToCustomerView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AddressToCustomerView'
GO
USE [master]
GO
ALTER DATABASE [DrugLandDB] SET  READ_WRITE 
GO
