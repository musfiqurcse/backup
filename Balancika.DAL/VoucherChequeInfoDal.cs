using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Dal.Base;

namespace Balancika.Dal
{
	public class VoucherChequeInfoDal : Balancika.Dal.Base.VoucherChequeInfoDalBase
	{
		public VoucherChequeInfoDal() : base()
		{
		}
        public int GetMaximumVoucherChequeInfoID()
        {
            try
            {
                int maxId = GetMaximumID("VoucherChequeInfo", "ChequeId", 0, "");
                return maxId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
	}
}
