using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Dal.Base;

namespace Balancika.Dal
{
	public class TempLedgerReportTableDal : Balancika.Dal.Base.TempLedgerReportTableDalBase
	{
		public TempLedgerReportTableDal() : base()
		{
		}
        public int GetMaxTempLedgerIdByMasterAccountId(Int64 masterId)
        {
            try
            {


                int maxId = GetMaximumID("TempLedgerReportTable", "VoucherTempId", 0, String.Format("where MasterAccountCOAid={0}", masterId));
                return maxId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int GetMaximumTempLedgerReportTableID()
        {
            try
            {
                int maxId = GetMaximumID("TempLedgerReportTable", "VoucherTempId", 0, "");
                return maxId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
	}
}
