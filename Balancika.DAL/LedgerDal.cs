using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Dal.Base;

namespace Balancika.Dal
{
	public class LedgerDal : Balancika.Dal.Base.LedgerDalBase
	{
		public LedgerDal() : base()
		{
		}
        public int GetMaximumLedgerID()
        {
            try
            {
                int maxId = GetMaximumID("Ledger", "LedgerId", 0, "");
                return maxId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet GetDataSetByCompanyId(Int32 CompanyId)
        {
            try
            {
                string condition= String.Format(" where CompanyId={0}",CompanyId);
                DataSet newDataset = GetDataSet("Ledger", "*", condition );
                return newDataset;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
	}
}
