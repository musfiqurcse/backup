using System;
using System.Text;
using System.Data;
using System.Collections;
using BALANCIKA.DAL.Base;

namespace BALANCIKA.DAL
{
	public class JournalDetailsDal : BALANCIKA.DAL.Base.JournalDetailsDalBase
	{
		public JournalDetailsDal() : base()
		{
		}
        public int GetMaxJournalDetailsId()
        {
            try
            {
                int maxId = GetMaximumID("JournalDetails", "JournalDetailsId", 0, "");
                return maxId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
	}
}
