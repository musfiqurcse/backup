using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Balancika.DAL;

namespace Balancika.Dal.Base
{
	public class VoucherDetailTempDalBase : SqlServerConnection
	{
		public DataTable GetAllVoucherDetailTemp(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("VoucherDetailTemp", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
        public DataTable GetAllVoucherDetailTempByUserId(Hashtable lstData)
        {
            string whereCondition = " where VoucherDetailTemp.userId = @userId ";
            DataTable dt = new DataTable();
            try
            {
                dt = GetDataTable("VoucherDetailTemp", "*", whereCondition, lstData);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

		public DataTable GetVoucherDetailTempById(Hashtable lstData)
		{
			string whereCondition = " where VoucherDetailTemp.Id = @Id ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("VoucherDetailTemp", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertVoucherDetailTemp(Hashtable lstData)
		{
			string sqlQuery ="Insert into VoucherDetailTemp (Id, userId, COAId, LineDesciption, Amount) values(@Id, @userId, @COAId, @LineDesciption, @Amount);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateVoucherDetailTemp(Hashtable lstData)
		{
			string sqlQuery = "Update VoucherDetailTemp set userId = @userId, COAId = @COAId, LineDesciption = @LineDesciption, Amount = @Amount where VoucherDetailTemp.Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteVoucherDetailTempById(Hashtable lstData)
		{
			string sqlQuery = "delete from  VoucherDetailTemp where Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
        public int DeleteAllVoucherDetailsTempByUserId(Hashtable lstData)
        {
            string sqlQuery = "delete from  VoucherDetailTemp where userId = @userId;";
            try
            {
                int success = ExecuteNonQuery(sqlQuery, lstData);
                return success;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
            }
        }
	}
}
