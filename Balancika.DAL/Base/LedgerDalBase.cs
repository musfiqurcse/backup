using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Balancika.DAL;

namespace Balancika.Dal.Base
{
	public class LedgerDalBase : SqlServerConnection
	{
		public DataTable GetAllLedger(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("Ledger", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetLedgerByLedgerId(Hashtable lstData)
		{
			string whereCondition = " where Ledger.LedgerId = @LedgerId ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("Ledger", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertLedger(Hashtable lstData)
		{
            string sqlQuery = "Insert into Ledger (LedgerId, TransactionNo, TransactionDate, VoucherType, VoucherId, AccountNo, ContraAccountNo, Debit, Credit, Description, UserId, LastUpdateTime, CompanyId) values(@LedgerId, @TransactionNo, @TransactionDate, @VoucherType, @VoucherId, @AccountNo, @ContraAccountNo, @Debit, @Credit, @Description, @UserId, @LastUpdateTime, @CompanyId);";
            //(LedgerId, TransactionNo, TransactionDate, VoucherType, VoucherId, AccountNo, ContraAccountNo, Debit, Credit, Description, UserId, LastUpdateTime, CompanyId)
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateLedger(Hashtable lstData)
		{
			string sqlQuery = "Update Ledger set TransactionNo = @TransactionNo, TransactionDate = @TransactionDate, VoucherType = @VoucherType, VoucherId = @VoucherId, AccountNo = @AccountNo, ContraAccountNo = @ContraAccountNo, Debit = @Debit, Credit = @Credit, Description = @Description, UserId = @UserId, LastUpdateTime = @LastUpdateTime, CompanyId = @CompanyId where Ledger.LedgerId = @LedgerId;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteLedgerByLedgerId(Hashtable lstData)
		{
			string sqlQuery = "delete from  Ledger where LedgerId = @LedgerId;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
