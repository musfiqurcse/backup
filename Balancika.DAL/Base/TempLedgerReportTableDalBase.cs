using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Balancika.DAL;

namespace Balancika.Dal.Base
{
	public class TempLedgerReportTableDalBase : SqlServerConnection
	{
		public DataTable GetAllTempLedgerReportTable(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("TempLedgerReportTable", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetTempLedgerReportTableByVoucherTempId(Hashtable lstData)
		{
			string whereCondition = " where TempLedgerReportTable.VoucherTempId = @VoucherTempId ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("TempLedgerReportTable", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertTempLedgerReportTable(Hashtable lstData)
		{
			string sqlQuery ="Insert into TempLedgerReportTable (VoucherTempId, VoucherTrasactionDate, MasterAccount, MasterAccountCOAid, ContraAccount, ContraAccountId, OpeningBalance, Debit, Credit, ClosingBalance, VoucherType, TransactionCode) values(@VoucherTempId, @VoucherTrasactionDate, @MasterAccount, @MasterAccountCOAid, @ContraAccount, @ContraAccountId, @OpeningBalance, @Debit, @Credit, @ClosingBalance, @VoucherType, @TransactionCode);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateTempLedgerReportTable(Hashtable lstData)
		{
			string sqlQuery = "Update TempLedgerReportTable set VoucherTrasactionDate = @VoucherTrasactionDate, MasterAccount = @MasterAccount, MasterAccountCOAid = @MasterAccountCOAid, ContraAccount = @ContraAccount, ContraAccountId = @ContraAccountId, OpeningBalance = @OpeningBalance, Debit = @Debit, Credit = @Credit, ClosingBalance = @ClosingBalance, VoucherType = @VoucherType, TransactionCode = @TransactionCode where TempLedgerReportTable.VoucherTempId = @VoucherTempId;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteTempLedgerReportTableByVoucherTempId(Hashtable lstData)
		{
			string sqlQuery = "delete from  TempLedgerReportTable where VoucherTempId = @VoucherTempId;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
