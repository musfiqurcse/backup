using System;
using System.Text;
using System.Data;
using System.Collections;
using Balancika.Dal.Base;

namespace Balancika.Dal
{
	public class VoucherDetailTempDal : Balancika.Dal.Base.VoucherDetailTempDalBase
	{
		public VoucherDetailTempDal() : base()
		{
		}

	    public int GetMaximumVoucherDetailTempId()
	    {
            try
            {
                int maxId = GetMaximumID("VoucherDetailTemp", "Id", 0, "");
                return maxId;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
	    }

	  
          
        
	}
}
